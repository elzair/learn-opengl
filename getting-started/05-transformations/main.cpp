// IO
#include <iostream>
#include <fstream>
#include <streambuf>
// GLEW
#define GLEW_STATIC
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>
// Math
#include "../../utils/glm/glm/glm.hpp"
#include "../../utils/glm/glm/gtc/matrix_transform.hpp"
#include "../../utils/glm/glm/gtc/type_ptr.hpp"
// Shader Handling
#include "../../utils/shader.hpp"
// Image Handling
#include <SOIL/SOIL.h>
  
void key_callback(GLFWwindow* window, int key, int scancode,
                  int action, int mode) {
  // When a user presses the escape key, we set WindowShouldClose
  // property to true, closing the application
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);
}

GLuint loadTexture(std::string path) {
  GLuint texture;
  glGenTextures(1, &texture);

  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	    // Horizontal texture wrapping
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);     // Vertical texture wrapping
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // Texture minimizing
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // Texture stretching
  int width, height;
  unsigned char* image = SOIL_load_image(path.c_str(), &width, &height, 0, SOIL_LOAD_RGB);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
  glGenerateMipmap(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, 0); 

  return texture;
}

int main() {
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL",
                                        nullptr, nullptr);
  if (window == nullptr) {
    std::cout << "Failed to create GLFW window" << std::endl;
    glfwTerminate();
    return -1;
  }
  glfwMakeContextCurrent(window);

  glewExperimental = GL_TRUE;
  if (glewInit() != GLEW_OK) {
    std::cerr << "Failed to initialize GLEW" << std::endl;
    glfwTerminate();
    return -1;
  }

  glfwSetKeyCallback(window, key_callback);  // Set keyboard callback function

  glViewport(0, 0, 800, 600);                // Define viewport dimenstions

  // Initialize shaders
  std::vector<ShaderInfo> shaders = {
    ShaderInfo("init.vert", GL_VERTEX_SHADER),
    ShaderInfo("init.frag", GL_FRAGMENT_SHADER)
  };
  std::vector<std::string> uniforms = {
    "ourTexture1", "ourTexture2", "transform"
  };

  // Load & compile shaders into program;
  Shader* program;
  try {
    program = new Shader(shaders, uniforms);
  }
  catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
    glfwTerminate();
    return -1;
  }

  // Initialize GPU memory
  GLfloat vertices[] = {
    // Positions          // Colors           // Texture Coords
     0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // Top Right
     0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // Bottom Right
    -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // Bottom Left
    -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // Top Left 
  };
  GLuint indices[] = {  // Note that we start from 0!
      0, 1, 3, // First Triangle
      1, 2, 3  // Second Triangle
  };

  // Create vertex array object
  GLuint VAO, VBO, EBO;
  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);
  glGenBuffers(1, &EBO);

  glBindVertexArray(VAO);                    // Bind VAO

  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // Draw in wireframe mode
  
  // Copy vertices array in a buffer for OpenGL to use
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices),
               vertices, GL_STATIC_DRAW);

  // Copy indices into buffer
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices),
               indices, GL_STATIC_DRAW);

  // Set Position Attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        8 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(0);
  // Set Color Attribute
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
                        8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
  glEnableVertexAttribArray(1);
  // Set Texture Coordinate Attribute
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE,
                        8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
  glEnableVertexAttribArray(2);  
  
  glBindVertexArray(0);                      // Unbind VAO

  // Create textures
  GLuint textures[2];
  textures[0] = loadTexture("../../resources/container.jpg");
  textures[1] = loadTexture("../../resources/awesomeface.png");

  while(!glfwWindowShouldClose(window)) {
    // Check and call events
    glfwPollEvents();

    // Rendering commands here
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // Set textures
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textures[0]);
    glUniform1i(program->uniforms["ourTexture1"], 0);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, textures[1]);
    glUniform1i(program->uniforms["ourTexture2"], 1);

    // Update rotation
    glm::mat4 trans;
    trans = glm::translate(trans, glm::vec3(0.5f, -0.5f, 0.0f));
    trans = glm::rotate(trans, glm::radians((GLfloat)glfwGetTime() * 50.0f),
                        glm::vec3(0.0, 0.0, 1.0));
    glUniformMatrix4fv(program->uniforms["transform"],
                       1, GL_FALSE, glm::value_ptr(trans));
    
    glUseProgram(program->id);             // Use shader program

    // Draw triangles
    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    // Swap the buffers
    glfwSwapBuffers(window);
  }

  // Properly de-allocate resources
  glDeleteVertexArrays(1, &VAO);
  glDeleteBuffers(1, &VBO);
  glDeleteBuffers(1, &EBO);
  glDeleteProgram(program->id);
  delete program;
  glfwTerminate();

  return 0;
}
