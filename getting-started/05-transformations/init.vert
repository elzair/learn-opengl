#version 330 core

layout (location = 0) in vec3 position; // The position variable has attribute position 0
layout (location = 1) in vec3 color;    // The color variable has attribute position 1
layout (location = 2) in vec2 texCoord; 

out vec3 ourColor; // Specify a color output to the fragment shader
out vec2 TexCoord;

uniform mat4 transform;

void main() {
  gl_Position = transform * vec4(position, 1.0f);
  ourColor    = color;
  TexCoord    = vec2(texCoord.x, 1.0 - texCoord.y);
}
