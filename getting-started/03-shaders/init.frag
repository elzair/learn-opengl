#version 330 core

in vec3 ourColor; // We set this variable in OpenGL
//in  vec4 vertexColor;
out vec4 color;

//uniform vec4 ourColor;

void main() {
  //color = vertexColor;
  //color = ourColor;
  color = vec4(ourColor, 1.0f);
}
