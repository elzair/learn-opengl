#version 330 core

layout (location = 0) in vec3 position; // The position variable has attribute position 0
layout (location = 1) in vec3 color;    // The color variable has attribute position 1

//out vec4 vertexColor;
out vec3 ourColor; // Specify a color output to the fragment shader

uniform float xoff;

void main() {
  //gl_Position = vec4(position, 1.0);
  //gl_Position = vec4(position.x, -position.y, position.z, 1.0);
  //gl_Position = vec4(position.x+xoff, position.y, position.z, 1.0);
  gl_Position = vec4(position.x+xoff, -position.y, position.z, 1.0);
  //vertexColor = vec4(0.5f, 0.0f, 0.0f, 1.0f);
  //ourColor = color;
  ourColor = position;
}
