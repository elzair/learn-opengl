// IO
#include <iostream>
#include <fstream>
#include <streambuf>
// GLEW
#define GLEW_STATIC
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>
// Math
#include "../../utils/glm/glm/glm.hpp"
#include "../../utils/glm/glm/gtc/matrix_transform.hpp"
#include "../../utils/glm/glm/gtc/type_ptr.hpp"
// Shader Handling
#include "../../utils/shader.hpp"
// Cube Handling
#include "../../utils/cube.hpp"
// Image Handling
#include <SOIL/SOIL.h>
// Camera Handling
#include "../../utils/camera.hpp"

// Define camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));

bool keys[1024];   // Define keymap

GLfloat lastX = 400, lastY = 300; // Initial mouse coordinates
bool firstMouse = true;

// Time variables
GLfloat deltaTime = 0.0f;      // Time between current frame  last frame
GLfloat lastFrame = 0.0f;      // Time of last frame

void do_movement() {
  GLint side = 0, front = 0;

  if (keys[GLFW_KEY_W] || keys[GLFW_KEY_UP]) {
    front = front + 1;
  }
  if (keys[GLFW_KEY_S] || keys[GLFW_KEY_DOWN]) {
    front = front - 1;
  }
  if (keys[GLFW_KEY_A] || keys[GLFW_KEY_LEFT]) {
    side = side - 1;
  }
  if (keys[GLFW_KEY_D] || keys[GLFW_KEY_RIGHT]) {
    side = side + 1;
  }

  if (front != 0 || side != 0) {
    camera.move(side, front, deltaTime);
  }
}
  
void key_callback(GLFWwindow* window, int key, int scancode,
                  int action, int mode) {
  // When a user presses the escape key, we set WindowShouldClose
  // property to true, closing the application
  if ((key == GLFW_KEY_ESCAPE || key == GLFW_KEY_Q) && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(window, GL_TRUE);
    return;
  }
  else if (action == GLFW_PRESS || action == GLFW_REPEAT) {
    keys[key] = true;
  }
  else {
    keys[key] = false;
  }
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
    if(firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }
  
    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos; // Reversed since y-coordinates go bottom->top
    lastX = xpos;
    lastY = ypos;

    camera.rotate(xoffset, yoffset);
}  

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
  camera.zoom((GLfloat) yoffset);
}

GLuint loadTexture(std::string path) {
  GLuint texture;
  glGenTextures(1, &texture);

  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	    // Horizontal texture wrapping
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);     // Vertical texture wrapping
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // Texture minimizing
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // Texture stretching
  int width, height;
  unsigned char* image = SOIL_load_image(path.c_str(), &width, &height, 0, SOIL_LOAD_RGB);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
  glGenerateMipmap(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, 0); 

  return texture;
}

int main() {
  int screenWidth = 800, screenHeight = 600;
  GLfloat aspect = ((GLfloat) screenWidth) / ((GLfloat) screenHeight);

  // Initialize window and make it current
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight,
                                        "LearnOpenGL", nullptr, nullptr);
  if (window == nullptr) {
    std::cout << "Failed to create GLFW window" << std::endl;
    glfwTerminate();
    return -1;
  }
  glfwMakeContextCurrent(window);

  glfwSetKeyCallback(window, key_callback);         // Set keyboard callback
  glfwSetCursorPosCallback(window, mouse_callback); // Set cursor movement callback
  glfwSetScrollCallback(window, scroll_callback);   // Set scrollwheel callback

  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); // Disable cursor in window

  // Initialize OpenGL extensions
  glewExperimental = GL_TRUE;
  if (glewInit() != GLEW_OK) {
    std::cerr << "Failed to initialize GLEW" << std::endl;
    glfwTerminate();
    return -1;
  }

  glViewport(0, 0, 800, 600);                // Define viewport dimenstions

  glEnable(GL_DEPTH_TEST);                   // Enable depth testing

  // Initialize shaders
  std::vector<ShaderInfo> shaders = {
    ShaderInfo("init.vert", GL_VERTEX_SHADER),
    ShaderInfo("init.frag", GL_FRAGMENT_SHADER)
  };
  std::vector<std::string> uniforms = {
    "ourTexture1", "ourTexture2", "model", "view", "projection"
  };

  // Load & compile shaders into program;
  Shader* program;
  try {
    program = new Shader(shaders, uniforms);
  }
  catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
    glfwTerminate();
    return -1;
  }

  // Initialize cube positions
  glm::vec3 cubePositions[] = {
    glm::vec3( 0.0f,  0.0f,  0.0f), 
    glm::vec3( 2.0f,  5.0f, -15.0f), 
    glm::vec3(-1.5f, -2.2f, -2.5f),  
    glm::vec3(-3.8f, -2.0f, -12.3f),  
    glm::vec3( 2.4f, -0.4f, -3.5f),  
    glm::vec3(-1.7f,  3.0f, -7.5f),  
    glm::vec3( 1.3f, -2.0f, -2.5f),  
    glm::vec3( 1.5f,  2.0f, -2.5f), 
    glm::vec3( 1.5f,  0.2f, -1.5f), 
    glm::vec3(-1.3f,  1.0f, -1.5f)  
  };

  // Create vertex array object
  GLuint VAO, VBO, EBO;
  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);
  glGenBuffers(1, &EBO);

  glBindVertexArray(VAO);                    // Bind VAO

  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // Draw in wireframe mode
  
  // Copy vertices array in a buffer for OpenGL to use
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  std::vector<GLfloat> vertices = Cube::draw(1.0, {"texcoords"});
  glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat),
               &vertices[0], GL_STATIC_DRAW);

  // Set Position Attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        5 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(0);
  // Set Texture Coordinate Attribute
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
                        5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
  glEnableVertexAttribArray(1);
  
  glBindVertexArray(0);                      // Unbind VAO

  // Create textures
  GLuint textures[2];
  textures[0] = loadTexture("../../resources/container.jpg");
  textures[1] = loadTexture("../../resources/awesomeface.png");

  while(!glfwWindowShouldClose(window)) {
    GLfloat currentFrame = glfwGetTime();
    deltaTime            = currentFrame - lastFrame;
    lastFrame            = currentFrame;
    
    glfwPollEvents();        // Check and call events
    do_movement();

    // Rendering commands here
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Set textures
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textures[0]);
    glUniform1i(program->uniforms["ourTexture1"], 0);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, textures[1]);
    glUniform1i(program->uniforms["ourTexture2"], 1);

    // Set view and projection matrices
    glm::mat4 view = camera.getView();
    glUniformMatrix4fv(program->uniforms["view"], 1, GL_FALSE,
                       glm::value_ptr(view));
    glm::mat4 projection = camera.getProjection(0.0f, aspect, 0.0f, 0.0f);
    glUniformMatrix4fv(program->uniforms["projection"], 1, GL_FALSE,
                       glm::value_ptr(projection));
    
    
    glUseProgram(program->id);             // Use shader program

    // Draw triangles
    glBindVertexArray(VAO);

    for (GLuint i = 0; i < 10; i++) {
      glm::mat4 model;
      model = glm::translate(model, cubePositions[i]);
      GLfloat angle = glm::radians(20.0f * i);
      model = glm::rotate(model, angle, glm::vec3(1.0f, 0.3f, 0.5f));

      glUniformMatrix4fv(program->uniforms["model"], 1, GL_FALSE,
                         glm::value_ptr(model));
      glDrawArrays(GL_TRIANGLES, 0, vertices.size());
    }

    glBindVertexArray(0);

    // Swap the buffers
    glfwSwapBuffers(window);
  }

  // Properly de-allocate resources
  glDeleteVertexArrays(1, &VAO);
  glDeleteBuffers(1, &VBO);
  glDeleteBuffers(1, &EBO);
  glDeleteProgram(program->id);
  delete program;
  glfwTerminate();

  return 0;
}
