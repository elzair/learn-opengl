// IO
#include <iostream>
#include <fstream>
#include <streambuf>
// GLEW
#define GLEW_STATIC
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>

struct shader_t {
  std::string path;
  GLenum type;
  GLuint num;
};
  
void key_callback(GLFWwindow* window, int key, int scancode,
                  int action, int mode) {
  // When a user presses the escape key, we set WindowShouldClose
  // property to true, closing the application
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);
}

void createShader(shader_t *shader) {
  std::ifstream shaderFile(shader->path.c_str());
  std::string shaderSource;
  
  // Load contents of shader file into a string
  if (shaderFile.is_open()) {
    shaderFile.seekg(0, std::ios::end);
    shaderSource.reserve(shaderFile.tellg());
    shaderFile.seekg(0, std::ios::beg);
    shaderSource.assign((std::istreambuf_iterator<char>(shaderFile)),
                         std::istreambuf_iterator<char>());
    shaderFile.close();
  }
  else 
    throw std::invalid_argument("shader file does not exist");
  const char *shaderText = shaderSource.c_str();

  // Compile shader
  shader->num = glCreateShader(shader->type);
  glShaderSource(shader->num, 1, &shaderText, NULL);
  glCompileShader(shader->num);
}

GLuint createProgram(shader_t *shaders, int num) {
  GLint success;
  GLchar infoLog[512];
  
  // Create all shaders
  for(int i = 0; i < num; i++) {
    createShader(&shaders[i]);
    glGetShaderiv(shaders[i].num, GL_COMPILE_STATUS, &success);
    
    if (!success) {
        glGetShaderInfoLog(shaders[i].num, 512, NULL, infoLog);
        throw std::runtime_error("Compilation of " + shaders[i].path +
                                 " failed\n" + infoLog + "\n");
    }
  }
  
  // Compile program
  GLuint shaderProgram = glCreateProgram();
  for (int i = 0; i < num; i++)
    glAttachShader(shaderProgram, shaders[i].num);
  glLinkProgram(shaderProgram);

  // Delete shaders
  for (int i = 0; i < num; i++) 
    glDeleteShader(shaders[i].num);

  // Check program creation succeeded
  glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
  if (!success) {
    glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
    throw std::runtime_error("Compilation of shader program failed\n" +
                             std::string(infoLog) + "\n");
  }

  return shaderProgram;
}

int main() {
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL",
                                        nullptr, nullptr);
  if (window == nullptr) {
    std::cout << "Failed to create GLFW window" << std::endl;
    glfwTerminate();
    return -1;
  }
  glfwMakeContextCurrent(window);

  glewExperimental = GL_TRUE;
  if (glewInit() != GLEW_OK) {
    std::cerr << "Failed to initialize GLEW" << std::endl;
    glfwTerminate();
    return -1;
  }

  glViewport(0, 0, 800, 600);

  glfwSetKeyCallback(window, key_callback);

  // Initialize GPU memory
  //GLfloat vertices[] = {
  //  -0.5f, -0.5f, 0.0f,
  //  0.5f, -0.5f, 0.0f,
  //  0.0f, 0.5f, 0.0f
  //};
  GLfloat vertices[] = {
       0.5f,  0.5f, 0.0f,  // Top Right
       0.5f, -0.5f, 0.0f,  // Bottom Right
      -0.5f, -0.5f, 0.0f,  // Bottom Left
      -0.5f,  0.5f, 0.0f   // Top Left 
  };
  GLuint indices[] = {  // Note that we start from 0!
      0, 1, 3,   // First Triangle
      1, 2, 3    // Second Triangle
  };  

  // Initialize shaders
  shader_t shaders[2] = {{"init.vert", GL_VERTEX_SHADER, 0},
                         {"init.frag", GL_FRAGMENT_SHADER, 0}};

  // Load & compile shaders into program;
  GLuint shaderProgram;
  try {
    shaderProgram = createProgram(shaders, 2);
  }
  catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
    glfwTerminate();
    return -1;
  }

  // Create vertex array object
  GLuint VAO, VBO, EBO;
  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);
  glGenBuffers(1, &EBO);

  glBindVertexArray(VAO);                    // Bind VAO

  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // Draw in wireframe mode
  
  // Copy vertices array in a buffer for OpenGL to use
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices),
               vertices, GL_STATIC_DRAW);

  // Copy indices into buffer
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices),
               indices, GL_STATIC_DRAW);

  // Set vertex attribute pointers
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        3 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(0);

  
  glBindVertexArray(0);                      // Unbind VAO

  while(!glfwWindowShouldClose(window)) {
    // Check and call events
    glfwPollEvents();

    // Rendering commands here
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // Use shader program
    glUseProgram(shaderProgram);

    // Draw triangles
    glBindVertexArray(VAO);
    //glDrawArrays(GL_TRIANGLES, 0, 3);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    // Swap the buffers
    glfwSwapBuffers(window);
  }

  glDeleteProgram(shaderProgram);
  glfwTerminate();
  return 0;
}
