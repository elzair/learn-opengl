// Std. Includes
#include <string>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// GL includes
#include "../utils/camera.hpp"
#include "../utils/cube.hpp"
#include "../utils/shader.hpp"
#include "../utils/model.hpp"

// Math
#include "../utils/glm/glm/glm.hpp"
#include "../utils/glm/glm/gtc/matrix_transform.hpp"
#include "../utils/glm/glm/gtc/type_ptr.hpp"

// Other Libs
#include <SOIL/SOIL.h>

// Properties
GLuint screenWidth = 800, screenHeight = 600;

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void do_movement();

// Camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

// The MAIN function, from here we start our application and run our Game loop
int main() {
  // Init GLFW
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "LearnOpenGL", nullptr, nullptr); // Windowed
  glfwMakeContextCurrent(window);

  // Set the required callback functions
  glfwSetKeyCallback(window, key_callback);
  glfwSetCursorPosCallback(window, mouse_callback);
  glfwSetScrollCallback(window, scroll_callback);

  // Options
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // Initialize GLEW to setup the OpenGL Function pointers
  glewExperimental = GL_TRUE;
  glewInit();

  // Define the viewport dimensions
  glViewport(0, 0, screenWidth, screenHeight);

  // Setup some OpenGL options
  glEnable(GL_DEPTH_TEST);

  // Setup and compile our shaders
  Shader *shader, * lamp;
  try {
    shader = new Shader({
        ShaderInfo("object.vert", GL_VERTEX_SHADER),
        ShaderInfo("object.frag", GL_FRAGMENT_SHADER)
      }, {
        "model", "view", "projection", "viewPos",
          "material.texture_diffuse1", "material.texture_specular1", "material.shininess",
          "pointLights[0].position", "pointLights[0].constant", "pointLights[0].linear", "pointLights[0].quadratic", "pointLights[0].ambient", "pointLights[0].diffuse", "pointLights[0].specular",
          "pointLights[1].position", "pointLights[1].constant", "pointLights[1].linear", "pointLights[1].quadratic", "pointLights[1].ambient", "pointLights[1].diffuse", "pointLights[1].specular",
        "dirLight.direction", "dirLight.ambient", "dirLight.diffuse", "dirLight.specular"
      }
    );
    lamp = new Shader({
        ShaderInfo("lampLight.vert", GL_VERTEX_SHADER),
        ShaderInfo("lampLight.frag", GL_FRAGMENT_SHADER)
      },
      {"model", "view", "projection"}
    );
  }
  catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
    glfwTerminate();
    return -1;
  }

  // Load models
  Model ourModel("../resources/models/nanosuit/nanosuit.obj");

  // Create VAO for light source
  glUseProgram(lamp->id);
  GLuint lightVAO, VBO;
  glGenVertexArrays(1, &lightVAO);
  glBindVertexArray(lightVAO);
  glGenBuffers(1, &VBO);

  // We only need to bind to the VBO, the container's VBO's data already contains the correct data.
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  std::vector<GLfloat> vertices = Cube::draw(1.0, {"normals", "texcoords"});
  glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat),
               &vertices[0], GL_STATIC_DRAW);
  // Set the vertex attributes (only position data for our lamp)
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        8 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(0);
  glBindVertexArray(0); 

  // Draw in wireframe
  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  glm::vec3 plPos[] = {
    glm::vec3(2.3f, -1.6f, -3.0f),
    glm::vec3(-1.7f, 0.9f, 1.0f)
  };

  // Game loop
  while(!glfwWindowShouldClose(window)) {
    // Set frame time
    GLfloat currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    // Check and call events
    glfwPollEvents();
    do_movement();

    // Clear the colorbuffer
    glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(shader->id);   // <-- Don't forget this one!

    // Transformation matrices
    GLfloat aspect = ((GLfloat)screenWidth) / ((GLfloat)screenHeight);
    glm::mat4 projection = camera.getProjection(0.0f, aspect, 0.1f, 100.0f);
    glm::mat4 view       = camera.getView();
    glm::vec3 viewPos    = camera.getPosition();
    glUniform3f(shader->uniforms["viewPos"], viewPos.x,  viewPos.y,  viewPos.z);
    glUniform1f(shader->uniforms["material.shininess"], 16.0f);
    glUniformMatrix4fv(shader->uniforms["projection"], 1, GL_FALSE, glm::value_ptr(projection));
    glUniformMatrix4fv(shader->uniforms["view"],       1, GL_FALSE, glm::value_ptr(view));

    glUniform3f(shader->uniforms["pointLights[0].position"],  plPos[0].x, plPos[0].y, plPos[0].z);
    glUniform3f(shader->uniforms["pointLights[0].ambient"],   0.5f,  0.5f,  0.5f);
    glUniform3f(shader->uniforms["pointLights[0].diffuse"],   1.0f,  1.0f,  1.0f);
    glUniform3f(shader->uniforms["pointLights[0].specular"],  1.0f,  1.0f,  1.0f);
    glUniform1f(shader->uniforms["pointLights[0].constant"],  1.0f);
    glUniform1f(shader->uniforms["pointLights[0].linear"],    0.009f);
    glUniform1f(shader->uniforms["pointLights[0].quadratic"], 0.0032);
    glUniform3f(shader->uniforms["pointLights[1].position"],  plPos[1].x, plPos[1].y, plPos[1].z);
    glUniform3f(shader->uniforms["pointLights[1].ambient"],   0.5f,  0.5f,  0.5f);
    glUniform3f(shader->uniforms["pointLights[1].diffuse"],   1.0f,  1.0f,  1.0f);
    glUniform3f(shader->uniforms["pointLights[1].specular"],  1.0f,  1.0f,  1.0f);
    glUniform1f(shader->uniforms["pointLights[1].constant"],  1.0f);
    glUniform1f(shader->uniforms["pointLights[1].linear"],    0.009f);
    glUniform1f(shader->uniforms["pointLights[1].quadratic"], 0.0032);

    // Draw the loaded model
    glm::mat4 model;
    model = glm::translate(model, glm::vec3(0.0f, -1.75f, 0.0f)); // Translate it down a bit so it's at the center of the scene
    model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));	// It's a bit too big for our scene, so scale it down
    glUniformMatrix4fv(shader->uniforms["model"],      1, GL_FALSE, glm::value_ptr(model));
    ourModel.Draw(shader);       

    // Draw lamps
    glUseProgram(lamp->id);
    glBindVertexArray(lightVAO);
    glUniformMatrix4fv(shader->uniforms["projection"], 1, GL_FALSE, glm::value_ptr(projection));
    glUniformMatrix4fv(shader->uniforms["view"],       1, GL_FALSE, glm::value_ptr(view));
    for (size_t i = 0; i < 2; i++) {
      model = glm::mat4();
      model = glm::translate(model, plPos[i]);
      model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
      glUniformMatrix4fv(lamp->uniforms["model"], 1, GL_FALSE, glm::value_ptr(model));
      glDrawArrays(GL_TRIANGLES, 0, 36);
    }

    // Swap the buffers
    glfwSwapBuffers(window);
  }

  glDeleteVertexArrays(1, &lightVAO);
  glDeleteBuffers(1, &VBO);
  glDeleteProgram(shader->id);
  glDeleteProgram(lamp->id);
  delete shader;
  delete lamp;
  glfwTerminate();

  return 0;
}

// Moves/alters the camera positions based on user input
void do_movement() {
  GLint side = 0, front = 0;

  if (keys[GLFW_KEY_W] || keys[GLFW_KEY_UP]) {
    front = front + 1;
  }
  if (keys[GLFW_KEY_S] || keys[GLFW_KEY_DOWN]) {
    front = front - 1;
  }
  if (keys[GLFW_KEY_A] || keys[GLFW_KEY_LEFT]) {
    side = side - 1;
  }
  if (keys[GLFW_KEY_D] || keys[GLFW_KEY_RIGHT]) {
    side = side + 1;
  }

  if (front != 0 || side != 0) {
    camera.move(side, front, deltaTime);
  }
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode) {
  if ((key == GLFW_KEY_ESCAPE || key == GLFW_KEY_Q) && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(window, GL_TRUE);
  }

  if(action == GLFW_PRESS)
    keys[key] = true;
  else if(action == GLFW_RELEASE)
    keys[key] = false;	
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
  if(firstMouse) {
    lastX = xpos;
    lastY = ypos;
    firstMouse = false;
  }

  GLfloat xoffset = xpos - lastX;
  GLfloat yoffset = lastY - ypos; 
  
  lastX = xpos;
  lastY = ypos;

  camera.rotate(xoffset, yoffset);
}	

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
  camera.zoom((GLfloat) yoffset);
}
