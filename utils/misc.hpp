#ifndef MISC_H
#define MISC_H

// IO
#include <iostream>
#include <fstream>
#include <streambuf>
// Data Structuers
#include <string>
#include <vector>
#include <list>
// Boost

namespace Misc {
  std::string readFile(std::string);

  template <typename T>
  std::vector<T> zip(std::list<std::vector<T> > list) {
    // Get total number of elements for all vectors
    size_t capacity = 0, max_vec_size = 0;

    for (auto i = list.begin(); i != list.end(); i++) {
      size_t vec_size = i->size();
      capacity += vec_size;
      max_vec_size = vec_size > max_vec_size ? vec_size : max_vec_size;
    }

    std::vector<T> res;
    res.reserve(capacity); // Reserve enough space to fit all elements

    for (size_t j = 0; j < max_vec_size; j++) {
      for (auto i = list.begin(); i != list.end(); i++) {
        res.emplace_back(i->at(j % i->size()));
      }
    }
    
    return res;
  }
}

#endif
