
#include "glm/glm/vec3.hpp"                  // GLM
#include "glm/glm/gtc/matrix_transform.hpp"  
#include "misc.hpp"                          // For zip()
#include "cube.hpp"

namespace Cube {
  std::vector<GLfloat> draw(GLfloat length,
                            std::initializer_list<std::string> options) {
    GLfloat c = length/2.0; // Center cube at (0,0,0)
    std::vector<GLfloat> posX = {
      // Front
      -c, c, -c, -c, c, c,
      // Back
      c, -c, c, c, -c, -c,
      // Left
      -c, -c, -c, -c, -c, -c,
      // Right
      c, c, c, c, c, c,
      // Top
      -c, c, -c, -c, c, c,
      // Bottom
      c, -c, c, c, -c, -c     
    };
    std::vector<GLfloat> posY = {
      // Front   
      -c, -c, c, c, -c, c,
      // Back
      -c, -c, c, c, -c, c,
      // Left
      -c, -c, c, c, -c, c,
      // Right
      -c, -c, c, c, -c, c,
      // Top
      c, c, c, c, c, c,
      // Bottom
      -c, -c, -c, -c, -c, -c
    };
    std::vector<GLfloat> posZ = {
      // Front      
      c, c, c, c, c, c,
      // Back
      -c, -c, -c, -c, -c, -c,
      // Left
      -c, c, -c, -c, c, c,
      // Right
      c, -c, c, c, -c, -c,
      // Top
      c, c, -c, -c, c, -c,
      // Bottom
      c, c, -c, -c, c, -c
    };
    std::vector<GLfloat> texPosS = {
      // Front    
      0.0, 1.0, 0.0, 0.0, 1.0, 1.0,
      // Back 
      0.0, 1.0, 0.0, 0.0, 1.0, 1.0,
      // Left
      0.0, 1.0, 0.0, 0.0, 1.0, 1.0,
      // Right 
      0.0, 1.0, 0.0, 0.0, 1.0, 1.0,
      // Top
      0.0, 1.0, 0.0, 0.0, 1.0, 1.0,
      // Bottom
      0.0, 1.0, 0.0, 0.0, 1.0, 1.0
    };
    std::vector<GLfloat> texPosT = {
      // Front
      0.0, 0.0, 1.0, 1.0, 0.0, 1.0,
      // Back
      0.0, 0.0, 1.0, 1.0, 0.0, 1.0,
      // Left
      0.0, 0.0, 1.0, 1.0, 0.0, 1.0,
      // Right
      0.0, 0.0, 1.0, 1.0, 0.0, 1.0,
      // Top
      0.0, 0.0, 1.0, 1.0, 0.0, 1.0,
      // Bottom
      0.0, 0.0, 1.0, 1.0, 0.0, 1.0
    }; 
    std::vector<GLfloat> normalX = {
      // Front
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Back
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Left
      -1.0, -1.0, -1.0, -1.0, -1.0, -1.0,
      // Right
      1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
      // Top
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Bottom
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    };
     std::vector<GLfloat> normalY = {
      // Front
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Back
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Left
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Right
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Top
      1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
      // Bottom
      -1.0, -1.0, -1.0, -1.0, -1.0, -1.0
    };
     std::vector<GLfloat> normalZ = {
      // Front
      1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
      // Back
      -1.0, -1.0, -1.0, -1.0, -1.0, -1.0,
      // Left
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Right
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Top
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Bottom
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    };

     // Get options
     std::list<std::vector<GLfloat>> vecs = {posX, posY, posZ};
     for (auto i = options.begin(); i != options.end(); i++) {
       if (*i == "texcoords") {
         vecs.emplace_back(texPosS);
         vecs.emplace_back(texPosT);
       }
       else if (*i == "normals") {
         vecs.emplace_back(normalX);
         vecs.emplace_back(normalY);
         vecs.emplace_back(normalZ);
       }
     }
  
     return Misc::zip(vecs); // Return zipped vectors
  }
  std::vector<GLfloat> drawout(GLfloat length,
                               std::initializer_list<std::string> options) {
    GLfloat c = length/2.0; // Center cube at (0,0,0)
    std::vector<GLfloat> posX = {
      // Front
      -c, -c, c, -c, c, c,
      // Back
      c, c, -c, c, -c, -c,
      // Left
      -c, -c, -c, -c, -c, -c,
      // Right
      c, c, c, c, c, c,
      // Top
      -c, -c, c, -c, c, c,
      // Bottom
      c, c, -c, c, -c, -c     
    };
    std::vector<GLfloat> posY = {
      // Front   
      -c, c, -c, c, c, -c,
      // Back
      -c, c, -c, c, c, -c,
      // Left
      -c, c, -c, c, c, -c,
      // Right
      -c, c, -c, c, c, -c,
      // Top
      c, c, c, c, c, c,
      // Bottom
      -c, -c, -c, -c, -c, -c
    };
    std::vector<GLfloat> posZ = {
      // Front      
      c, c, c, c, c, c,
      // Back
      -c, -c, -c, -c, -c, -c,
      // Left
      -c, -c, c, -c, c, c,
      // Right
      c, c, -c, c, -c, -c,
      // Top
      c, -c, c, -c, -c, c,
      // Bottom
      c, -c, c, -c, -c, c
    };
    std::vector<GLfloat> texPosS = {
      // Front    
      0.0, 0.0, 1.0, 0.0, 1.0, 1.0,
      // Back 
      0.0, 0.0, 1.0, 0.0, 1.0, 1.0,
      // Left
      0.0, 0.0, 1.0, 0.0, 1.0, 1.0,
      // Right 
      0.0, 0.0, 1.0, 0.0, 1.0, 1.0,
      // Top
      0.0, 0.0, 1.0, 0.0, 1.0, 1.0,
      // Bottom
      0.0, 0.0, 1.0, 0.0, 1.0, 1.0
    };
    std::vector<GLfloat> texPosT = {
      // Front
      0.0, 1.0, 0.0, 1.0, 1.0, 0.0,
      // Back
      0.0, 1.0, 0.0, 1.0, 1.0, 0.0,
      // Left
      0.0, 1.0, 0.0, 1.0, 1.0, 0.0,
      // Right
      0.0, 1.0, 0.0, 1.0, 1.0, 0.0,
      // Top
      0.0, 1.0, 0.0, 1.0, 1.0, 0.0,
      // Bottom
      0.0, 1.0, 0.0, 1.0, 1.0, 0.0
    }; 
    // Start here
    std::vector<GLfloat> normalX = {
      // Front
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Back
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Left
      -1.0, -1.0, -1.0, -1.0, -1.0, -1.0,
      // Right
      1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
      // Top
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Bottom
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    };
     std::vector<GLfloat> normalY = {
      // Front
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Back
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Left
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Right
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Top
      1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
      // Bottom
      -1.0, -1.0, -1.0, -1.0, -1.0, -1.0
    };
     std::vector<GLfloat> normalZ = {
      // Front
      1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
      // Back
      -1.0, -1.0, -1.0, -1.0, -1.0, -1.0,
      // Left
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Right
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Top
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      // Bottom
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    };

     // Get options
     std::list<std::vector<GLfloat>> vecs = {posX, posY, posZ};
     for (auto i = options.begin(); i != options.end(); i++) {
       if (*i == "texcoords") {
         vecs.emplace_back(texPosS);
         vecs.emplace_back(texPosT);
       }
       else if (*i == "normals") {
         vecs.emplace_back(normalX);
         vecs.emplace_back(normalY);
         vecs.emplace_back(normalZ);
       }
     }
  
     return Misc::zip(vecs); // Return zipped vectors
  }
}
