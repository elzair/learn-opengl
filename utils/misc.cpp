#include "misc.hpp"

namespace Misc {
  std::string readFile(std::string path) {
    std::ifstream file(path.c_str());
    std::string text;
    
    // Load contents of shader file into a string
    if (file.is_open()) {
      file.seekg(0, std::ios::end);
      text.reserve(file.tellg());
      file.seekg(0, std::ios::beg);
      text.assign((std::istreambuf_iterator<char>(file)),
                  std::istreambuf_iterator<char>());
      file.close();
    }
    else 
      throw std::invalid_argument("File does not exist: " + path);

    return text;
   
  }
}
