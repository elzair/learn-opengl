#ifndef CUBE_H
#define CUBE_H

#include <GL/glew.h>         // OpenGL bindings
#include <vector>
#include <string>
#include <initializer_list>

namespace Cube {
  std::vector<GLfloat> draw(GLfloat, std::initializer_list<std::string>);
  std::vector<GLfloat> drawout(GLfloat, std::initializer_list<std::string>);
}

#endif
