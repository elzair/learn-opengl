#ifndef SHADER_H
#define SHADER_H

// Data Structures
#include <vector>
#include <string>
#include <unordered_map>
  
#include <GL/glew.h> // Include glew to get all the required OpenGL headers

struct ShaderInfo {
  std::string              path;
  GLenum                   type;
  std::string              text;

  // Constructor for shader with source code stored in file
  ShaderInfo(std::string i_path, GLenum i_type)
    : path(std::move(i_path)), type(std::move(i_type)), text("")
    {}
  // Constructor for shader with source code already in string
  ShaderInfo(GLenum i_type, std::string i_text)
    : path(""), type(std::move(i_type)), text(std::move(i_text))
    {}
};

class Shader {
  GLuint create(ShaderInfo);
public:
  GLuint                                 id;
  std::unordered_map<std::string,GLuint> uniforms;
  std::unordered_map<std::string,GLuint> attributes;
  Shader(std::vector<ShaderInfo>,
         std::vector<std::string>);
};


#endif
