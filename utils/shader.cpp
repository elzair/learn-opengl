#include <string>
#include <vector>
#include <stdexcept>  // Exceptions
#include "misc.hpp"   // File Support
#include "shader.hpp"

GLuint Shader::create(ShaderInfo shader) {
  std::string shaderSource;
  
  // If necessary, read shader source from file
  if (shader.path.length() > 0) 
    shaderSource = Misc::readFile(shader.path);
  else 
    shaderSource = shader.text;
  
  const char *shaderText = shaderSource.c_str();

  // Compile shader
  GLuint num = glCreateShader(shader.type);
  glShaderSource(num, 1, &shaderText, NULL);
  glCompileShader(num);

  return num;
}

Shader::Shader(std::vector<ShaderInfo> shaders,
               std::vector<std::string> uniforms) {
  GLint success;
  GLchar infoLog[512];
  std::vector<GLuint> ids;
  
  // Create all shaders
  for(auto it = shaders.begin(); it != shaders.end(); ++it) {
    GLuint shaderID = this->create(*it);
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &success);
    ids.emplace_back(shaderID);
    
    if (!success) {
        glGetShaderInfoLog(shaderID, 512, NULL, infoLog);
        throw std::runtime_error("Compilation of " + it->path +
                                 " failed\n" + infoLog + "\n");
    }
  }
  
  // Create Program and attach shaders
  this->id = glCreateProgram();
  for (auto it = ids.begin(); it != ids.end(); ++it)
    glAttachShader(this->id, *it);
  
  glLinkProgram(this->id);      // Compile program

  // Delete shaders
  for (auto it = ids.begin(); it != ids.end(); ++it) 
    glDeleteShader(*it);

  // Check program creation succeeded
  glGetProgramiv(this->id, GL_LINK_STATUS, &success);
  if (!success) {
    glGetProgramInfoLog(this->id, 512, NULL, infoLog);
    throw std::runtime_error("Compilation of shader program failed\n" +
                             std::string(infoLog) + "\n");
  }

  glUseProgram(this->id);       // Set program to current to enable attributes

  // Retrieve location of uniforms
  for (auto it = uniforms.begin(); it != uniforms.end(); ++it) {
    this->uniforms[*it] = glGetUniformLocation(this->id, it->c_str());
  }

  glUseProgram(0);                // Deactivate program until it is needed
}
