#ifndef CAMERA_H
#define CAMERA_H

#include <GL/glew.h>                         // OpenGL bindings
#include <vector>
#include "glm/glm/vec3.hpp"                  // GLM
#include "glm/glm/gtc/matrix_transform.hpp"  
#include <stdexcept>                         // Exceptions

class Camera {
  // Camera Vectors
  glm::vec3 position;
  glm::vec3 front;
  glm::vec3 up;
  glm::vec3 right;
  glm::vec3 worldUp;
  // Euler Angles
  GLfloat yaw;
  GLfloat pitch;
  // Options
  GLfloat sensitivity;
  GLfloat speed;
  // Projection Variables
  GLfloat fov;
  GLfloat aspect;
  GLfloat near;
  GLfloat far;
  // Methods
  void updateCameraVectors();

 public:
  Camera(glm::vec3 i_position      = glm::vec3(0.0f, 0.0f, 0.0f),
         glm::vec3 i_up            = glm::vec3(0.0f, 1.0f, 0.0f),
         GLfloat   i_yaw           = -90.0f,
         GLfloat   i_pitch         = 0.0f,
         GLfloat   i_sensitivity   = 0.25f,
         GLfloat   i_speed         = 3.0f) 
           : front(glm::vec3(0.0f, 0.0f, -1.0f)) {
    this->position      = i_position;
    this->worldUp       = i_up;
    this->yaw           = i_yaw;
    this->pitch         = i_pitch;
    this->sensitivity   = i_sensitivity;
    this->speed         = i_speed;
    this->fov           = 45.0f;
    this->aspect        = 1.0f;
    this->near          = 0.1f;
    this->far           = 100.0f;
    this->updateCameraVectors();
  }
  // API
  void move(GLint, GLint, GLfloat);
  void rotate(GLfloat, GLfloat);
  void zoom(GLfloat);
  glm::mat4 getView();
  glm::mat4 getProjection(GLfloat, GLfloat, GLfloat, GLfloat);
  glm::vec3 getPosition();
  glm::vec3 getFront();
  void setPosition(glm::vec3);
};

#endif
