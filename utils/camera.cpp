#include <GL/glew.h>                         // OpenGL bindings
#include <cmath>
#include <vector>
#include "glm/glm/vec3.hpp"                  // GLM
#include "glm/glm/gtc/matrix_transform.hpp"  
#include <stdexcept>                         // Exceptions
#include "camera.hpp"                        // My header

void Camera::updateCameraVectors() {
  // Calculate the new Front vector
  glm::vec3 front;
  front.x = cos(glm::radians(this->yaw)) * cos(glm::radians(this->pitch));
  front.y = sin(glm::radians(this->pitch));
  front.z = sin(glm::radians(this->yaw)) * cos(glm::radians(this->pitch));
  this->front = glm::normalize(front);
  // Also re-calculate the Right and Up vector
  this->right = glm::normalize(glm::cross(this->front, this->worldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
  this->up    = glm::normalize(glm::cross(this->right, this->front));
}

void Camera::move(GLint sideSign,
                  GLint frontSign,
                  GLfloat deltaTime) {
  const GLfloat DIV_SQRT_2 = 0.70710678118654752440; // 1/sqrt(2)

  // Prevent division by zero
  if (sideSign == 0 && frontSign == 0) {
    throw std::runtime_error("Do not specify (0,0) for Camera::move()");
  }
  
  // Also ensure both sideSign & frontSign are -1, 0, or 1
  sideSign  = (((GLint) 0) < sideSign)  - (sideSign < ((GLint) 0));
  frontSign = (((GLint) 0) < frontSign) - (frontSign < ((GLint) 0));

  // Convert both signs to components
  GLfloat sideComponent  = (GLfloat) sideSign;
  GLfloat frontComponent = (GLfloat) frontSign;

  // Ensure components are normalized to the unit vector
  if (sideComponent != 0.0 && frontComponent != 0.0) {
    sideComponent  = sideComponent  * DIV_SQRT_2;
    frontComponent = frontComponent * DIV_SQRT_2;
  }
  
  GLfloat sideVelocity  = sideComponent  * this->speed;
  GLfloat frontVelocity = frontComponent * this->speed;

  this->position = this->position +
                   this->right * sideVelocity  * deltaTime +
                   this->front * frontVelocity * deltaTime;
}

void Camera::rotate(GLfloat deltaYaw, GLfloat deltaPitch) {
  const GLfloat MAX_PITCH_ANGLE = 89.0f;

  // Reduce offset to allow smoother movement
  deltaYaw   = deltaYaw   * this->sensitivity;
  deltaPitch = deltaPitch * this->sensitivity;

  this->yaw   = this->yaw   + deltaYaw;
  this->pitch = this->pitch + deltaPitch;

  // Constrain pitch angle to -pi/2 < 0 < pi/2
  if (this->pitch > MAX_PITCH_ANGLE) {
      this->pitch = MAX_PITCH_ANGLE;
  }
  if (this->pitch < -MAX_PITCH_ANGLE) {
      this->pitch = -MAX_PITCH_ANGLE;
  }

  this->updateCameraVectors(); // Update vectors with the new pitch and yaw
}

void Camera::zoom(GLfloat deltaFov) {
  if (this->fov >= 1.0f && this->fov <= 45.0f) {
    this->fov -= deltaFov;
  }
  else if (this->fov < 1.0f) {
    this->fov = 1.0f;
  }
  else if (this->fov > 45.0f) {
    this->fov = 45.0f;
  }
}

glm::mat4 Camera::getView() {
  return glm::lookAt(this->position, this->position + this->front, this->up);
}

glm::mat4 Camera::getProjection(GLfloat i_fov    = 0.0f,
                                GLfloat i_aspect = 0.0f,
                                GLfloat i_near   = 0.0f,
                                GLfloat i_far    = 0.0f) {
  this->fov    = i_fov    != 0.0f ? i_fov    : this->fov;
  this->aspect = i_aspect != 0.0f ? i_aspect : this->aspect;
  this->near   = i_near   != 0.0f ? i_near   : this->near;
  this->far    = i_far    != 0.0f ? i_far    : this->far;

  return glm::perspective(glm::radians(this->fov),
                          this->aspect,
                          this->near,
                          this->far);
}

glm::vec3 Camera::getPosition() {
  return this->position;
}

glm::vec3 Camera::getFront() {
  return this->front;
}

void Camera::setPosition(glm::vec3 position) {
  this->position = position;
}
