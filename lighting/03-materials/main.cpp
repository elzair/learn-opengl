// IO
#include <iostream>
#include <fstream>
#include <streambuf>
// GLEW
#define GLEW_STATIC
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>
// Math
#include "../../utils/glm/glm/glm.hpp"
#include "../../utils/glm/glm/gtc/matrix_transform.hpp"
#include "../../utils/glm/glm/gtc/type_ptr.hpp"
// Shader Handling
#include "../../utils/shader.hpp"
// Cube Handling
#include "../../utils/cube.hpp"
// Image Handling
#include <SOIL/SOIL.h>
// Camera Handling
#include "../../utils/camera.hpp"

// Define camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));

bool keys[1024];   // Define keymap

GLfloat lastX = 400, lastY = 300; // Initial mouse coordinates
bool firstMouse = true;

// Time variables
GLfloat deltaTime = 0.0f;      // Time between current frame  last frame
GLfloat lastFrame = 0.0f;      // Time of last frame

// Light attributes
glm::vec3 lightPos(1.2f, 1.0f, 2.0f);

GLuint loadTexture(std::string path) {
  GLuint texture;
  glGenTextures(1, &texture);

  glBindTexture(GL_TEXTURE_2D, texture);
  int width, height;
  unsigned char* image = SOIL_load_image(path.c_str(), &width, &height, 0, SOIL_LOAD_RGB);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
  glGenerateMipmap(GL_TEXTURE_2D);
  SOIL_free_image_data(image);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	                   // Horizontal texture wrapping
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);                    // Vertical texture wrapping
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST); // Texture minimizing
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST); // Texture stretching
  glBindTexture(GL_TEXTURE_2D, 0); 

  return texture;
}

void do_movement() {
  GLint side = 0, front = 0;

  if (keys[GLFW_KEY_W] || keys[GLFW_KEY_UP]) {
    front = front + 1;
  }
  if (keys[GLFW_KEY_S] || keys[GLFW_KEY_DOWN]) {
    front = front - 1;
  }
  if (keys[GLFW_KEY_A] || keys[GLFW_KEY_LEFT]) {
    side = side - 1;
  }
  if (keys[GLFW_KEY_D] || keys[GLFW_KEY_RIGHT]) {
    side = side + 1;
  }

  if (front != 0 || side != 0) {
    camera.move(side, front, deltaTime);
  }
}
  
void key_callback(GLFWwindow* window, int key, int scancode,
                  int action, int mode) {
  // When a user presses the escape key, we set WindowShouldClose
  // property to true, closing the application
  if ((key == GLFW_KEY_ESCAPE || key == GLFW_KEY_Q) && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(window, GL_TRUE);
    return;
  }
  else if (action == GLFW_PRESS || action == GLFW_REPEAT) {
    keys[key] = true;
  }
  else {
    keys[key] = false;
  }
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if(firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }
  
    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos; // Reversed since y-coordinates go bottom->top
    lastX = xpos;
    lastY = ypos;

    camera.rotate(xoffset, yoffset);
}  

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
  camera.zoom((GLfloat) yoffset);
}

int main() {
  int screenWidth = 800, screenHeight = 600;
  GLfloat aspect = ((GLfloat) screenWidth) / ((GLfloat) screenHeight);

  // Initialize window and make it current
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight,
                                        "LearnOpenGL", nullptr, nullptr);
  if (window == nullptr) {
    std::cout << "Failed to create GLFW window" << std::endl;
    glfwTerminate();
    return -1;
  }
  glfwMakeContextCurrent(window);

  glfwSetKeyCallback(window, key_callback);         // Set keyboard callback
  glfwSetCursorPosCallback(window, mouse_callback); // Set cursor movement callback
  glfwSetScrollCallback(window, scroll_callback);   // Set scrollwheel callback

  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); // Disable cursor in window

  // Initialize OpenGL extensions
  glewExperimental = GL_TRUE;
  if (glewInit() != GLEW_OK) {
    std::cerr << "Failed to initialize GLEW" << std::endl;
    glfwTerminate();
    return -1;
  }

  glViewport(0, 0, 800, 600);                // Define viewport dimenstions

  glEnable(GL_DEPTH_TEST);                   // Enable depth testing

  // Initialize shaders
  std::vector<ShaderInfo> lightingShaders = {
    ShaderInfo("lighting.vert", GL_VERTEX_SHADER),
    ShaderInfo("lighting.frag", GL_FRAGMENT_SHADER)
  };
  std::vector<std::string> lightingUniforms = {
    "model", "view", "projection", "texCoords", "viewPos",
    "material.diffuse", "material.specular",
    "material.emission", "material.shininess",
    "light.ambient", "light.diffuse",
    "light.specular", "light.position"
  };
  std::vector<ShaderInfo> lampShaders = {
    ShaderInfo("lamp.vert", GL_VERTEX_SHADER),
    ShaderInfo("lamp.frag", GL_FRAGMENT_SHADER)
  };
  std::vector<std::string> lampUniforms = {
    "model", "view", "projection"
  };

  // Load & compile shaders into lightingShader;
  Shader * lightingShader, * lampShader;
  try {
    lightingShader = new Shader(lightingShaders, lightingUniforms);
    lampShader     = new Shader(lampShaders,     lampUniforms);
  }
  catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
    glfwTerminate();
    return -1;
  }

  // Create vertex array for lighting
  glUseProgram(lightingShader->id);
  GLuint containerVAO, VBO;
  glGenVertexArrays(1, &containerVAO);
  glGenBuffers(1, &VBO);

  glBindVertexArray(containerVAO);                    // Bind containerVAO

  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // Draw in wireframe mode
  
  // Copy vertices array in a buffer for OpenGL to use
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  std::vector<GLfloat> vertices = Cube::draw(1.0, {"normals", "texcoords"});
  glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat),
               &vertices[0], GL_STATIC_DRAW);

  // Set Position Attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        8 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(0);
  // Set Normal Attribute
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
                        8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
  glEnableVertexAttribArray(1);
  // Set TexCoords Attribute
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE,
                        8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
  glEnableVertexAttribArray(2);
  
  glBindVertexArray(0);                      // Unbind containerVAO

  // Create VAO for light source
  glUseProgram(lampShader->id);
  GLuint lightVAO;
  glGenVertexArrays(1, &lightVAO);
  glBindVertexArray(lightVAO);
  // We only need to bind to the VBO, the container's VBO's data already contains the correct data.
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  // Set the vertex attributes (only position data for our lamp)
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        8 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(0);
  glBindVertexArray(0); 

  // Create textures
  glUseProgram(lightingShader->id);
  GLuint diffuseMap   = loadTexture("../../resources/container2.png");
  GLuint specularMap  = loadTexture("../../resources/container2_specular.png");
  GLuint emissionMap  = loadTexture("../../resources/matrix.jpg");
  glUniform1i(lightingShader->uniforms["material.diffuse"],  0);
  glUniform1i(lightingShader->uniforms["material.specular"], 1);
  glUniform1i(lightingShader->uniforms["material.emission"], 2);

  while(!glfwWindowShouldClose(window)) {
    GLfloat currentFrame = glfwGetTime();
    deltaTime            = currentFrame - lastFrame;
    lastFrame            = currentFrame;
    
    glfwPollEvents();        // Check and call events
    do_movement();

    // Rendering commands here
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /**
     * Object Section
     */

    glUseProgram(lightingShader->id);             // Use shader lightingShader

    // Set lighting parameters
    glm::vec3 viewPos = camera.getPosition();
    glUniform3f(lightingShader->uniforms["viewPos"],
                viewPos.x,  viewPos.y,  viewPos.z);
    glUniform3f(lightingShader->uniforms["light.ambient"],
                0.2f, 0.2f, 0.2f);
    glUniform3f(lightingShader->uniforms["light.diffuse"],
                0.5f, 0.5f, 0.5f);
    glUniform3f(lightingShader->uniforms["light.specular"],
                1.0f, 1.0f, 1.0f);
    glUniform3f(lightingShader->uniforms["light.position"],
                lightPos.x, lightPos.y, lightPos.z);
    glUniform1f(lightingShader->uniforms["material.shininess"], 64.0f);

    // Set camera coordinates
    glm::mat4 view = camera.getView();
    glUniformMatrix4fv(lightingShader->uniforms["view"], 1, GL_FALSE,
                       glm::value_ptr(view));
    glm::mat4 projection = camera.getProjection(0.0f, aspect, 0.0f, 0.0f);
    glUniformMatrix4fv(lightingShader->uniforms["projection"], 1, GL_FALSE,
                       glm::value_ptr(projection));

    // Set lightmaps
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, diffuseMap);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, specularMap);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, emissionMap);

    // Draw object
    glBindVertexArray(containerVAO);
    glm::mat4 model;
    glUniformMatrix4fv(lightingShader->uniforms["model"],
                       1, GL_FALSE, glm::value_ptr(model));
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);

    /**
     * Light Source Section
     */

    glUseProgram(lampShader->id);             // Use shader lampShader

    glUniformMatrix4fv(lampShader->uniforms["view"], 1, GL_FALSE,
                       glm::value_ptr(view));
    glUniformMatrix4fv(lampShader->uniforms["projection"], 1, GL_FALSE,
                       glm::value_ptr(projection));
    model = glm::mat4();
    model = glm::translate(model, lightPos);
    model = glm::scale(model, glm::vec3(0.2f));
    glUniformMatrix4fv(lampShader->uniforms["model"], 1, GL_FALSE,
                       glm::value_ptr(model));

    // Draw the light object
    glBindVertexArray(lightVAO);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);
    
    glfwSwapBuffers(window);                       // Swap the buffers
  }

  // Properly de-allocate resources
  glDeleteVertexArrays(1, &containerVAO);
  glDeleteVertexArrays(1, &lightVAO);
  glDeleteBuffers(1, &VBO);
  glDeleteProgram(lightingShader->id);
  glDeleteProgram(lampShader->id);
  delete lightingShader;
  delete lampShader;
  glfwTerminate();

  return 0;
}
