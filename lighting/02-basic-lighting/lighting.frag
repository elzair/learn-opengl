#version 330 core
in vec3 FragPos;
in vec3 Normal;

out vec4 color;
  
uniform vec3 objectColor;
uniform vec3 lightColor;
uniform vec3 lightPos;
uniform vec3 viewPos;

void main()
{
  //color = vec4(lightColor * objectColor, 1.0f);
  // Calculate ambient color
  float ambientStrength  = 0.1f;
  vec3 ambient           = ambientStrength * lightColor;

  // Calculate diffuse color
  vec3 norm              = normalize(Normal);
  vec3 lightDir          = normalize(lightPos - FragPos);
  float diff             = max(dot(norm, lightDir), 0.0);
  vec3 diffuse           = diff * lightColor;

  // Calculate specular color
  float specularStrength = 0.5f;
  vec3 viewDir           = normalize(viewPos - FragPos);
  vec3 reflectDir        = reflect(-lightDir, norm);
  float spec             = pow(max(dot(viewDir, reflectDir), 0.0), 32);
  vec3 specular          = specularStrength * spec * lightColor;

  vec3 result            = (ambient + diffuse + specular) * objectColor;
  color                  = vec4(result, 1.0f);
}
