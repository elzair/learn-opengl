// Data Structures
#include <vector>
#include <string>
// IO
#include <iostream>
// GLEW
#define GLEW_STATIC
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>
// Math
#include "../../utils/glm/glm/glm.hpp"
#include "../../utils/glm/glm/gtc/matrix_transform.hpp"
#include "../../utils/glm/glm/gtc/type_ptr.hpp"
// Shader Handling
#include "../../utils/shader.hpp"
// Cube Handling
#include "../../utils/cube.hpp"
// Image Handling
#include <SOIL/SOIL.h>
// Camera Handling
#include "../../utils/camera.hpp"

// Define camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));

bool keys[1024];   // Define keymap

int     screenWidth  = 800;
int     screenHeight = 600;
// Initial mouse coordinates
GLfloat lastX        = (GLfloat)(screenWidth  / 2.0);
GLfloat lastY        = (GLfloat)(screenHeight / 2.0);
bool    firstMouse   = true;

// Time variables
GLfloat deltaTime = 0.0f;      // Time between current frame  last frame
GLfloat lastFrame = 0.0f;      // Time of last frame

// Initialize cube positions
std::vector<glm::vec3> cubePositions = {
  glm::vec3( 0.0f,  0.0f,  0.0f), 
  glm::vec3( 2.0f,  5.0f, -15.0f), 
  glm::vec3(-1.5f, -2.2f, -2.5f),  
  glm::vec3(-3.8f, -2.0f, -12.3f),  
  glm::vec3( 2.4f, -0.4f, -3.5f),  
  glm::vec3(-1.7f,  3.0f, -7.5f),  
  glm::vec3( 1.3f, -2.0f, -2.5f),  
  glm::vec3( 1.5f,  2.0f, -2.5f), 
  glm::vec3( 1.5f,  0.2f, -1.5f), 
  glm::vec3(-1.3f,  1.0f, -1.5f)  
};

// Initialize point light positions
std::vector<glm::vec3> pointLightPositions = {
	glm::vec3( 0.7f,  0.2f,  2.0f),
	glm::vec3( 2.3f, -3.3f, -4.0f),
	glm::vec3(-4.0f,  2.0f, -12.0f),
	glm::vec3( 0.0f,  0.0f, -3.0f)
};
GLfloat constantParam  = 1.0f;
GLfloat linearParam    = 0.09f;
GLfloat quadraticParam = 0.032f;


GLuint loadTexture(std::string path) {
  GLuint texture;
  glGenTextures(1, &texture);

  glBindTexture(GL_TEXTURE_2D, texture);
  int width, height;
  unsigned char* image = SOIL_load_image(path.c_str(), &width, &height, 0, SOIL_LOAD_RGB);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
  glGenerateMipmap(GL_TEXTURE_2D);
  SOIL_free_image_data(image);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	                   // Horizontal texture wrapping
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);                    // Vertical texture wrapping
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST); // Texture minimizing
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST); // Texture stretching
  glBindTexture(GL_TEXTURE_2D, 0); 

  return texture;
}

void do_movement() {
  GLint side = 0, front = 0;

  if (keys[GLFW_KEY_W] || keys[GLFW_KEY_UP]) {
    front = front + 1;
  }
  if (keys[GLFW_KEY_S] || keys[GLFW_KEY_DOWN]) {
    front = front - 1;
  }
  if (keys[GLFW_KEY_A] || keys[GLFW_KEY_LEFT]) {
    side = side - 1;
  }
  if (keys[GLFW_KEY_D] || keys[GLFW_KEY_RIGHT]) {
    side = side + 1;
  }

  if (front != 0 || side != 0) {
    camera.move(side, front, deltaTime);
  }
}
  
void key_callback(GLFWwindow* window, int key, int scancode,
                  int action, int mode) {
  // When a user presses the escape key, we set WindowShouldClose
  // property to true, closing the application
  if ((key == GLFW_KEY_ESCAPE || key == GLFW_KEY_Q) && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(window, GL_TRUE);
    return;
  }
  else if (action == GLFW_PRESS || action == GLFW_REPEAT) {
    keys[key] = true;
  }
  else {
    keys[key] = false;
  }
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if(firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }
  
    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos; // Reversed since y-coordinates go bottom->top
    lastX = xpos;
    lastY = ypos;

    camera.rotate(xoffset, yoffset);
}  

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
  camera.zoom((GLfloat) yoffset);
}

int main() {
  GLfloat aspect = ((GLfloat) screenWidth) / ((GLfloat) screenHeight);

  // Initialize window and make it current
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight,
                                        "LearnOpenGL", nullptr, nullptr);
  if (window == nullptr) {
    std::cout << "Failed to create GLFW window" << std::endl;
    glfwTerminate();
    return -1;
  }
  glfwMakeContextCurrent(window);

  glfwSetKeyCallback(window, key_callback);         // Set keyboard callback
  glfwSetCursorPosCallback(window, mouse_callback); // Set cursor movement callback
  glfwSetScrollCallback(window, scroll_callback);   // Set scrollwheel callback

  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); // Disable cursor in window

  // Initialize OpenGL extensions
  glewExperimental = GL_TRUE;
  if (glewInit() != GLEW_OK) {
    std::cerr << "Failed to initialize GLEW" << std::endl;
    glfwTerminate();
    return -1;
  }

  glViewport(0, 0, 800, 600);                // Define viewport dimenstions

  glEnable(GL_DEPTH_TEST);                   // Enable depth testing

  // Initialize shaders
  std::vector<ShaderInfo> multiShaders = {
    ShaderInfo("multiLight.vert", GL_VERTEX_SHADER),
    ShaderInfo("multiLight.frag", GL_FRAGMENT_SHADER)
  };
  std::vector<std::string> multiUniforms = {
    "model", "view", "projection", "texCoords", "viewPos",
    "material.diffuse", "material.specular",
    /*"material.emission",*/ "material.shininess",
    "dirLight.direction", "dirLight.ambient",
    "dirLight.diffuse", "dirLight.specular",
    "flashLight.position", "flashLight.direction",
    "flashLight.ambient", "flashLight.diffuse",
    "flashLight.specular", "flashLight.constant",
    "flashLight.linear", "flashLight.quadratic",
    "flashLight.innerCutoff", "flashLight.outerCutoff"
  };
  for (int i = 0; i < pointLightPositions.size(); i++) {
    std::string is = std::to_string(i);
    multiUniforms.emplace_back("pointLights[" + is + "].position");
    multiUniforms.emplace_back("pointLights[" + is + "].constant");
    multiUniforms.emplace_back("pointLights[" + is + "].linear");
    multiUniforms.emplace_back("pointLights[" + is + "].quadratic");
    multiUniforms.emplace_back("pointLights[" + is + "].ambient");
    multiUniforms.emplace_back("pointLights[" + is + "].diffuse");
    multiUniforms.emplace_back("pointLights[" + is + "].specular");
  }
  std::vector<ShaderInfo> smoothFLShaders = {
    ShaderInfo("smoothFlashLight.vert", GL_VERTEX_SHADER),
    ShaderInfo("smoothFlashLight.frag", GL_FRAGMENT_SHADER)
  };
  std::vector<std::string> smoothFLUniforms = {
    "model", "view", "projection", "texCoords", "viewPos",
    "material.diffuse", "material.specular",
    /*"material.emission",*/ "material.shininess",
    "light.ambient", "light.diffuse",
    "light.specular", "light.position", "light.direction",
    "light.innerCutoff", "light.outerCutoff",
    "light.constant", "light.linear", "light.quadratic"
  };
  std::vector<std::string> smoothFLAttributes = {
    "position", "normal", "texCoords"
  };
  std::vector<ShaderInfo> lampShaders = {
    ShaderInfo("lampLight.vert", GL_VERTEX_SHADER),
    ShaderInfo("lampLight.frag", GL_FRAGMENT_SHADER)
  };
  std::vector<std::string> lampUniforms = {
    "model", "view", "projection"
  };
  std::vector<std::string> lampAttributes = {
    "position"
  };

  // Load & compile shaders into directionShader;
  Shader * multiShader, * lampShader;
  try {
    multiShader = new Shader(multiShaders, multiUniforms);
    lampShader  = new Shader(lampShaders,  lampUniforms);
  }
  catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
    glfwTerminate();
    return -1;
  }

  // Create vertex array for lighting
  glUseProgram(multiShader->id);
  GLuint containerVAO, VBO;
  glGenVertexArrays(1, &containerVAO);
  glGenBuffers(1, &VBO);

  glBindVertexArray(containerVAO);                    // Bind containerVAO

  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // Draw in wireframe mode
  
  // Copy vertices array in a buffer for OpenGL to use
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  std::vector<GLfloat> vertices = Cube::draw(1.0, {"normals", "texcoords"});
  glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat),
               &vertices[0], GL_STATIC_DRAW);

  // Set Position Attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        8 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(0);
  // Set Normal Attribute
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
                        8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
  glEnableVertexAttribArray(1);
  // Set TexCoords Attribute
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE,
                        8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
  glEnableVertexAttribArray(2);

  glBindVertexArray(0);                      // Unbind containerVAO

  // Create VAO for light source
  glUseProgram(lampShader->id);
  GLuint lightVAO;
  glGenVertexArrays(1, &lightVAO);
  glBindVertexArray(lightVAO);
  // We only need to bind to the VBO, the container's VBO's data already contains the correct data.
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  // Set the vertex attributes (only position data for our lamp)
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        8 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(0);
  glBindVertexArray(0); 

  // Create textures
  glUseProgram(multiShader->id);
  GLuint diffuseMap   = loadTexture("../../resources/container2.png");
  GLuint specularMap  = loadTexture("../../resources/container2_specular.png");
  //GLuint emmisionMap = loadTexture("matrix.jpg");
  glUniform1i(multiShader->uniforms["material.diffuse"],  0);
  glUniform1i(multiShader->uniforms["material.specular"], 1);
  //glUniform1i(multiShader->uniforms["material.emission"], 2);

  while(!glfwWindowShouldClose(window)) {
    GLfloat currentFrame = glfwGetTime();
    deltaTime            = currentFrame - lastFrame;
    lastFrame            = currentFrame;
    
    glfwPollEvents();        // Check and call events
    do_movement();

    // Rendering commands here
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /**
     * Object Section
     */

    glUseProgram(multiShader->id);             // Use shader pointShader

    // Set lighting parameters
    glm::vec3 viewPos = camera.getPosition();
    glm::vec3 viewDir = camera.getFront();
    glUniform1f(multiShader->uniforms["material.shininess"], 32.0f);
    glUniform3f(multiShader->uniforms["viewPos"], viewPos.x,  viewPos.y,  viewPos.z);
    // Set uniforms for directional lighting
    glUniform3f(multiShader->uniforms["dirLight.direction"], -0.2f, -1.0f, -0.3f);
    glUniform3f(multiShader->uniforms["dirLight.ambient"],  0.2f, 0.2f, 0.2f);
    glUniform3f(multiShader->uniforms["dirLight.diffuse"],  0.5f, 0.5f, 0.5f);
    glUniform3f(multiShader->uniforms["dirLight.specular"], 1.0f, 1.0f, 1.0f);
    // Set uniforms for point lights
    for (int i = 0; i < pointLightPositions.size(); i++) {
      std::string is = std::to_string(i);
      glUniform3f(multiShader->uniforms["pointLights[" + is + "].position"],
                  pointLightPositions[i].x, pointLightPositions[i].y, pointLightPositions[i].z);
      glUniform1f(multiShader->uniforms["pointLights[" + is + "].constant"],  constantParam);
      glUniform1f(multiShader->uniforms["pointLights[" + is + "].linear"],    linearParam);
      glUniform1f(multiShader->uniforms["pointLights[" + is + "].quadratic"], quadraticParam);
    }
    // Set uniforms for flashlight
    glUniform3f(multiShader->uniforms["flashLight.direction"], viewDir.x, viewDir.y, viewDir.z);
    glUniform3f(multiShader->uniforms["flashLight.position"], viewPos.x, viewPos.y, viewPos.z);
    glUniform3f(multiShader->uniforms["flashLight.ambient"], 0.0f, 0.0f, 0.0f);
    glUniform3f(multiShader->uniforms["flashLight.diffuse"], 1.0f, 1.0f, 1.0f);
    glUniform3f(multiShader->uniforms["flashLight.specular"], 1.0f, 1.0f, 1.0f);
    glUniform1f(multiShader->uniforms["flashLight.constant"],  constantParam);
    glUniform1f(multiShader->uniforms["flashLight.linear"],    linearParam);
    glUniform1f(multiShader->uniforms["flashLight.quadratic"], quadraticParam);
    // Pass cosine of cutoff angle to avoid doing inverse cosine operation in fragment shader
    glUniform1f(multiShader->uniforms["flashLight.innerCutoff"], glm::cos(glm::radians(12.5f)));
    glUniform1f(multiShader->uniforms["flashLight.outerCutoff"], glm::cos(glm::radians(17.5f)));

    // Set camera coordinates
    glm::mat4 view = camera.getView();
    glm::mat4 projection = camera.getProjection(0.0f, aspect, 0.1f, 100.0f);
    glUniformMatrix4fv(multiShader->uniforms["view"], 1, GL_FALSE,
                       glm::value_ptr(view));
    glUniformMatrix4fv(multiShader->uniforms["projection"], 1, GL_FALSE,
                       glm::value_ptr(projection));

    // Set lightmaps
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, diffuseMap);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, specularMap);
    //glActiveTexture(GL_TEXTURE2);
    //glBindTexture(GL_TEXTURE_2D, emissionMap);

    // Draw object
    glBindVertexArray(containerVAO);
    glm::mat4 model;
    for (GLuint i = 0; i < cubePositions.size(); i++) {
      model = glm::mat4();
      model = glm::translate(model, cubePositions[i]);
      GLfloat angle = glm::radians(20.0f * i);
      model = glm::rotate(model, angle, glm::vec3(1.0f, 0.3f, 0.5f));
      glUniformMatrix4fv(multiShader->uniforms["model"], 1, GL_FALSE,
                         glm::value_ptr(model));
      glDrawArrays(GL_TRIANGLES, 0, vertices.size());
    }
    glBindVertexArray(0);

    /**
     * Light Source Section
     */

    glUseProgram(lampShader->id);             // Use shader lampShader
    glBindVertexArray(lightVAO);

    glUniformMatrix4fv(lampShader->uniforms["view"], 1, GL_FALSE,
                    glm::value_ptr(view));
    glUniformMatrix4fv(lampShader->uniforms["projection"], 1, GL_FALSE,
                    glm::value_ptr(projection));
    for (size_t i = 0; i < pointLightPositions.size(); i++) {
      model = glm::mat4();
      model = glm::translate(model, pointLightPositions[i]);
      model = glm::scale(model, glm::vec3(0.2f));
      glUniformMatrix4fv(lampShader->uniforms["model"], 1, GL_FALSE,
                      glm::value_ptr(model));
      // Draw the light object
      glDrawArrays(GL_TRIANGLES, 0, 36);
    }

    glBindVertexArray(0);
    
    glfwSwapBuffers(window);                       // Swap the buffers
  }

  // Properly de-allocate resources
  glDeleteVertexArrays(1, &containerVAO);
  glDeleteVertexArrays(1, &lightVAO);
  glDeleteBuffers(1, &VBO);
  glDeleteProgram(multiShader->id);
  glDeleteProgram(lampShader->id);
  delete multiShader;
  delete lampShader;
  glfwTerminate();

  return 0;
}
