#version 330 core

struct Material {
  sampler2D diffuse;
  sampler2D specular;
  //sampler2D emission;
  float     shininess;
};

struct DirLight {
  vec3      direction;

  vec3      ambient;
  vec3      diffuse;
  vec3      specular;
};

struct PointLight {
  vec3      position;

  float     constant;
  float     linear;
  float     quadratic;
  
  vec3      ambient;
  vec3      diffuse;
  vec3      specular;
};

struct FlashLight {
  vec3      position;
  vec3      direction;
  float     innerCutoff;
  float     outerCutoff;

  float     constant;
  float     linear;
  float     quadratic;
  
  vec3      ambient;
  vec3      diffuse;
  vec3      specular;
};
  
#define NR_POINT_LIGHTS 4

in      vec3       FragPos;
in      vec3       Normal;
in      vec2       TexCoords;

out     vec4       color;

uniform vec3       viewPos;
uniform DirLight   dirLight;
uniform PointLight pointLights[NR_POINT_LIGHTS];
uniform FlashLight flashLight;
uniform Material   material;

// Function prototypes
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);
vec3 CalcFlashLight(FlashLight light, vec3 normal, vec3 fragPos, vec3 viewDir);

void main() {
  // Properties
  vec3  norm        = normalize(Normal);
  vec3  viewDir     = normalize(viewPos - FragPos);

  // Phase 1: Directional lighting
  vec3  result      = CalcDirLight(dirLight, norm, viewDir);

  // Phase 2: Point lights
  for(int i=0; i < NR_POINT_LIGHTS; i++)
    result += CalcPointLight(pointLights[i], norm, FragPos, viewDir);

  // Phase 3: Flashlight
  result += CalcFlashLight(flashLight, norm, FragPos, viewDir);
  
  //color             = vec4(ambient + diffuse + specular + emission, 1.0f);
  color             = vec4(result, 1.0);
}

// Calculate shading component of Directional light
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir) {
   vec3 lightDir    = normalize(-light.direction);
   
  // Calculate Diffuse color 
  float diff        = max(dot(normal, lightDir), 0.0);
  
  // Calculate Specular color
  vec3  reflectDir  = reflect(-lightDir, normal);
  float spec        = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
  
  // Combine results
  vec3  ambient     = light.ambient  * vec3(texture(material.diffuse, TexCoords));
  vec3  diffuse     = light.diffuse  * diff * vec3(texture(material.diffuse, TexCoords));
  vec3  specular    = light.specular * spec * vec3(texture(material.specular, TexCoords));

  return ambient + diffuse + specular;
}

// Calculate shading component of a Point Light
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir) {
  vec3  lightDir    = normalize(light.position - fragPos);
  
  // Calculate Diffuse color 
  float diff        = max(dot(normal, lightDir), 0.0);
  
  // Calculate Specular color
  vec3 reflectDir   = reflect(-lightDir, normal);
  float spec        = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
  
  // Factor in Attenuation
  float distance    = length(light.position - fragPos);
  float attenuation = 1.0f / (light.constant + light.linear * distance + 
			                light.quadratic * (distance * distance));
  
  // Combine results
  vec3  ambient     = light.ambient  * vec3(texture(material.diffuse, TexCoords));
  vec3  diffuse     = light.diffuse  * diff * vec3(texture(material.diffuse, TexCoords));
  vec3  specular    = light.specular * spec * vec3(texture(material.specular, TexCoords));
  
  ambient  *= attenuation;
  diffuse  *= attenuation;
  specular *= attenuation;
  
  return ambient + diffuse + specular;
} 

// Calculate shading component of flashlight
vec3 CalcFlashLight(FlashLight light, vec3 normal, vec3 fragPos, vec3 viewDir) {
  //// Calculate emission color
  //vec3  emission         = vec3(texture(material.emission, TexCoords));

  // Calculate Ambient color 
  vec3  ambient     = light.ambient  * vec3(texture(material.diffuse, TexCoords));
  
  // Calculate diffuse color
  vec3  lightDir    = normalize(light.position - FragPos);
  float diff        = max(dot(normal, lightDir), 0.0);
  vec3  diffuse     = light.diffuse * diff * vec3(texture(material.diffuse, TexCoords));

  // Calculate specular color
  vec3  reflectDir  = reflect(-lightDir, normal);
  float spec        = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
  vec3  specular    = light.specular * spec * vec3(texture(material.specular, TexCoords));

  // Make soft spotlight edges
  // Since lightDir and -light.direction will be unit vectors
  // A . B = |A| * |B| * cos(theta) = 1*1*cos(theta) = cos(Theta)
  float cosTheta    = dot(lightDir, normalize(-light.direction));
  float epsilon     = light.innerCutoff - light.outerCutoff;
  float intensity   = clamp((cosTheta - light.outerCutoff) / epsilon, 0.0, 1.0); 
  diffuse  *= intensity;
  specular *= intensity;

  // Factor in attenuation
  float distance    = length(light.position - FragPos);
  float attenuation = 1.0f / (light.constant + light.linear * distance +
                                   light.quadratic * (distance * distance));
  diffuse  *= attenuation;
  specular *= attenuation;

  return ambient + diffuse + specular;
}
