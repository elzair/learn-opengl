#version 330 core

struct Material {
  sampler2D diffuse;
  sampler2D specular;
  //sampler2D emission;
  float     shininess;
};

struct Light {
  vec3      position;
  vec3      direction;
  float     cutoff;

  vec3      ambient;
  vec3      diffuse;
  vec3      specular;

  float     constant;
  float     linear;
  float     quadratic;
};
  
in      vec3     FragPos;
in      vec3     Normal;
in      vec2     TexCoords;

out     vec4     color;

uniform vec3     viewPos;
uniform Material material;
uniform Light    light;

void main()
{
  // Calculate ambient color
  vec3  ambient          = light.ambient * vec3(texture(material.diffuse, TexCoords));

  //// Calculate emission color
  //vec3  emission         = vec3(texture(material.emission, TexCoords));

  // Check if fragment is inside the spotlight cone
  vec3  lightDir         = normalize(light.position - FragPos);
  // Since lightDir and -light.direction will be unit vectors
  // A . B = |A| * |B| * cos(theta) = 1*1*cos(theta) = cos(Theta)
  float cosTheta         = dot(lightDir, normalize(-light.direction));
  
  // Cos(Theta) is inversely proportional to Theta from 0 < Theta < Pi/2
  if(cosTheta > light.cutoff) { 
    // Calculate diffuse color
    vec3  norm             = normalize(Normal);
    float diff             = max(dot(norm, lightDir), 0.0);
    vec3  diffuse          = light.diffuse * diff * vec3(texture(material.diffuse, TexCoords));

    // Calculate specular color
    vec3  viewDir          = normalize(viewPos - FragPos);
    vec3  reflectDir       = reflect(-lightDir, norm);
    float spec             = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3  specular         = light.specular * spec * vec3(texture(material.specular, TexCoords));

    // Factor in attenuation
    float distance         = length(light.position - FragPos);
    float attenuation      = 1.0f / (light.constant + light.linear * distance +
                                     light.quadratic * (distance * distance));
    diffuse  *= attenuation;
    specular *= attenuation;

    color                  = vec4(ambient + diffuse + specular, 1.0f);
  }
  else {
    color                  = vec4(ambient, 1.0f);
  }
}
