#version 330 core

in      vec3        Normal;
in      vec3        Position;

out     vec4        color;

uniform vec3        viewPos;
uniform samplerCube skybox;

void main() {
  float ratio = 1.00 / 1.52;  // Ratio of refractive indices of air and glass
  vec3  I     = normalize(Position - viewPos);
  vec3  R     = refract(I, normalize(Normal), ratio);
  color       = texture(skybox, R);
}
