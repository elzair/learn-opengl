#include <iostream>

// Std. Includes
#include <string>
#include <map>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// GL includes
#include "../../utils/camera.hpp"
#include "../../utils/cube.hpp"
#include "../../utils/shader.hpp"

// Math
#include "../../utils/glm/glm/glm.hpp"
#include "../../utils/glm/glm/gtc/matrix_transform.hpp"
#include "../../utils/glm/glm/gtc/type_ptr.hpp"

// Model loading
#include "../../utils/model.hpp"

// Other Libs
#include <SOIL/SOIL.h>

std::vector<GLfloat> points = {
	-0.5f,  0.5f, // Top-left
	 0.5f,  0.5f, // Top-right
	 0.5f, -0.5f, // Bottom-right
	-0.5f, -0.5f  // Bottom-left
};

std::vector<GLfloat> pointsAndColors = {
    -0.5f,  0.5f, 1.0f, 0.0f, 0.0f, // Top-left
     0.5f,  0.5f, 0.0f, 1.0f, 0.0f, // Top-right
     0.5f, -0.5f, 0.0f, 0.0f, 1.0f, // Bottom-right
    -0.5f, -0.5f, 1.0f, 1.0f, 0.0f  // Bottom-left
};  


// Properties
GLuint screenWidth = 800, screenHeight = 600;

// Camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

// Function prototypes
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);
void mouseCallback(GLFWwindow* window, double xpos, double ypos);
void doMovement();

int main(int argc, char** argv) {
  int    geom_type  = 0;

  // Get reflect argument (if present)
  if (argc >= 2) { 
    std::string::size_type sz;

    geom_type = std::stoi(std::string(argv[1]), &sz, 10);

    if (geom_type < 0 || geom_type > 5) {
      std::cerr << "Invalid geometry argument!" << std::endl;
      return -2;
    }
  }
  
  /*
   * Window & OpenGL Initialization
   */
  
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "LearnOpenGL", nullptr, nullptr); // Windowed
  glfwMakeContextCurrent(window);

  // Set the required callback functions
  glfwSetKeyCallback(window, keyCallback);
  glfwSetCursorPosCallback(window, mouseCallback);
  glfwSetScrollCallback(window, scrollCallback);

  // Options
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // Initialize GLEW to setup the OpenGL Function pointers
  glewExperimental = GL_TRUE;
  glewInit();

  // Define the viewport dimensions
  glViewport(0, 0, screenWidth, screenHeight);

  /*
   * Shader Initialization
   */

  // Setup and compile our shaders
  Shader* shader, * houseShader, * coloredHouseShader, * snowyHouseShader, * modelShader, * modelExplodeShader, *modelNormalShader, * shaderp, * shader2p;
  try {
    shader = new Shader({
        ShaderInfo("primary.vert", GL_VERTEX_SHADER),
        ShaderInfo("primary.geom", GL_GEOMETRY_SHADER),
        ShaderInfo("primary.frag", GL_FRAGMENT_SHADER)
      },
      {});
    houseShader = new Shader({
        ShaderInfo("house.vert", GL_VERTEX_SHADER),
        ShaderInfo("house.geom", GL_GEOMETRY_SHADER),
        ShaderInfo("house.frag", GL_FRAGMENT_SHADER)
      },
      {});
    coloredHouseShader = new Shader({
        ShaderInfo("colored-house.vert", GL_VERTEX_SHADER),
        ShaderInfo("colored-house.geom", GL_GEOMETRY_SHADER),
        ShaderInfo("colored-house.frag", GL_FRAGMENT_SHADER)
      },
      {});
    snowyHouseShader = new Shader({
        ShaderInfo("snowy-house.vert", GL_VERTEX_SHADER),
        ShaderInfo("snowy-house.geom", GL_GEOMETRY_SHADER),
        ShaderInfo("snowy-house.frag", GL_FRAGMENT_SHADER)
      },
      {});
    modelShader = new Shader({
        ShaderInfo("model.vert", GL_VERTEX_SHADER),
        ShaderInfo("model.frag", GL_FRAGMENT_SHADER)
      },
      {
        "model", "view", "projection",
        "material.texture_diffuse1", "material.texture_specular1"
    });
    modelExplodeShader = new Shader({
        ShaderInfo("model-explode.vert", GL_VERTEX_SHADER),
        ShaderInfo("model-explode.geom", GL_GEOMETRY_SHADER),
        ShaderInfo("model-explode.frag", GL_FRAGMENT_SHADER)
      },
      {
        "model", "view", "projection", "time",
        "material.texture_diffuse1", "material.texture_specular1"
      });
    modelNormalShader = new Shader({
        ShaderInfo("model-normal.vert", GL_VERTEX_SHADER),
        ShaderInfo("model-normal.geom", GL_GEOMETRY_SHADER),
        ShaderInfo("model-normal.frag", GL_FRAGMENT_SHADER)
      },
      {
        "model", "view", "projection",
        "material.texture_diffuse1", "material.texture_specular1"
      });

  }
  catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
    glfwTerminate();
    return -1;
  }

  std::vector<GLfloat> point_vertices;
  //std::vector<GLfloat>* pointp;
  GLsizei point_stride;
  switch(geom_type) {
  case 0:
    shaderp        = shader;
    shader2p       = NULL;
    point_vertices = points;
    point_stride   = 2 * sizeof(GLfloat);
    break;
  case 1:
    shaderp        = houseShader;
    shader2p       = NULL;
    point_vertices = points;
    point_stride   = 2 * sizeof(GLfloat);
    break;
  case 2:
    shaderp        = coloredHouseShader;
    shader2p       = NULL;
    point_vertices = pointsAndColors;
    point_stride   = 5 * sizeof(GLfloat);
    break;
  case 3:
    shaderp        = snowyHouseShader;
    shader2p       = NULL;
    point_vertices = pointsAndColors;
    point_stride   = 5 * sizeof(GLfloat);
    break;
  case 4:
    shaderp        = modelExplodeShader;
    shader2p       = NULL;
    glEnable(GL_DEPTH_TEST);
    break;
  case 5:
    shaderp        = modelShader;
    shader2p       = modelNormalShader;
    glEnable(GL_DEPTH_TEST);
    break;
  }

  /*
   * Vertex Array & Vertex Buffer Initialization
   */

  GLuint pointVAO, pointVBO;
  if (geom_type >= 0 && geom_type <= 3) {
    glGenVertexArrays(1, &pointVAO);
    glGenBuffers(1, &pointVBO);
    glBindVertexArray(pointVAO);
    glBindBuffer(GL_ARRAY_BUFFER, pointVBO);
    glBufferData(GL_ARRAY_BUFFER, point_vertices.size() * sizeof(GLfloat),
                 &point_vertices[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
                          point_stride, (GLvoid*)0);
    if (geom_type == 2 || geom_type == 3) {
      glEnableVertexAttribArray(1);
      glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
                            point_stride, (GLvoid*)(2 * sizeof(GLfloat)));
    }
    glBindVertexArray(0);
  }

  /*
   * 3D Model Initialization
   */

  Model ourModel("../../resources/models/nanosuit/nanosuit.obj");

  /*
   * Main Loop
   */ 

  while(!glfwWindowShouldClose(window)) {
    // Set frame time
    GLfloat currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    // Check and call events
    glfwPollEvents();
    doMovement();

    // Clear the colorbuffer and enable depth testing
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    if (geom_type < 4) {
      glClear(GL_COLOR_BUFFER_BIT);
    }
    else {
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    // Draw objects
    glUseProgram(shaderp->id);   // <-- Don't forget this one!

    // Draw the object 
    if (geom_type == 4 || geom_type == 5) {
      GLfloat aspect       = ((GLfloat)screenWidth) / ((GLfloat)screenHeight);
      glm::mat4 projection = camera.getProjection(0.0f, aspect, 0.1f, 100.0f);
      glm::mat4 view  = camera.getView();
      glm::mat4 model = glm::mat4();
      auto      time  = glfwGetTime();
      glUniformMatrix4fv(shaderp->uniforms["projection"], 1, GL_FALSE, glm::value_ptr(projection));
      glUniformMatrix4fv(shaderp->uniforms["view"],  1, GL_FALSE, glm::value_ptr(view));
      glUniformMatrix4fv(shaderp->uniforms["model"], 1, GL_FALSE, glm::value_ptr(model));
      if (geom_type == 4) {
        glUniform1f(shaderp->uniforms["time"], time);
      }
      ourModel.Draw(shaderp);

      if (geom_type == 5) {
        glUseProgram(modelNormalShader->id);
        glUniformMatrix4fv(modelNormalShader->uniforms["projection"], 1, GL_FALSE, glm::value_ptr(projection));
        glUniformMatrix4fv(modelNormalShader->uniforms["view"],  1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(modelNormalShader->uniforms["model"], 1, GL_FALSE, glm::value_ptr(model));
        ourModel.Draw(modelNormalShader);
      }
    }
    else {
      glBindVertexArray(pointVAO);
      glDrawArrays(GL_POINTS, 0, 4);
      glBindVertexArray(0);
    }

    // Swap the buffers
    glfwSwapBuffers(window);
  }

  // Free resources
  glDeleteVertexArrays(1, &pointVAO);
  glDeleteProgram(shader->id);
  glDeleteProgram(houseShader->id);
  glDeleteProgram(coloredHouseShader->id);
  glDeleteProgram(snowyHouseShader->id);
  glDeleteProgram(modelExplodeShader->id);
  glDeleteProgram(modelShader->id);
  glDeleteProgram(modelNormalShader->id);
  delete shader;
  delete houseShader;
  delete coloredHouseShader;
  delete snowyHouseShader;
  delete modelExplodeShader;
  delete modelShader;
  delete modelNormalShader;
  glfwTerminate();

  return 0;
}

// Moves/alters the camera positions based on user input
void doMovement() {
  GLint side = 0, front = 0;

  if (keys[GLFW_KEY_W] || keys[GLFW_KEY_UP]) {
    front = front + 1;
  }
  if (keys[GLFW_KEY_S] || keys[GLFW_KEY_DOWN]) {
    front = front - 1;
  }
  if (keys[GLFW_KEY_A] || keys[GLFW_KEY_LEFT]) {
    side = side - 1;
  }
  if (keys[GLFW_KEY_D] || keys[GLFW_KEY_RIGHT]) {
    side = side + 1;
  }

  if (front != 0 || side != 0) {
    camera.move(side, front, deltaTime);
  }
}

// Is called whenever a key is pressed/released via GLFW
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode) {
  if((key == GLFW_KEY_ESCAPE || key == GLFW_KEY_Q) && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);

  if(action == GLFW_PRESS)
    keys[key] = true;
  else if(action == GLFW_RELEASE)
    keys[key] = false;	
}

void mouseCallback(GLFWwindow* window, double xpos, double ypos) {
  if(firstMouse) {
    lastX = xpos;
    lastY = ypos;
    firstMouse = false;
  }

  GLfloat xoffset = xpos - lastX;
  GLfloat yoffset = lastY - ypos; 
  
  lastX = xpos;
  lastY = ypos;

  camera.rotate(xoffset, yoffset);
}	

void scrollCallback(GLFWwindow* window, double xoffset, double yoffset) {
  camera.zoom((GLfloat) yoffset);
}
