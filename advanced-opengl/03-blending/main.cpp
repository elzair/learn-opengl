#include <iostream>

// Std. Includes
#include <string>
#include <map>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// GL includes
#include "../../utils/camera.hpp"
#include "../../utils/cube.hpp"
#include "../../utils/shader.hpp"

// Math
#include "../../utils/glm/glm/glm.hpp"
#include "../../utils/glm/glm/gtc/matrix_transform.hpp"
#include "../../utils/glm/glm/gtc/type_ptr.hpp"

// Other Libs
#include <SOIL/SOIL.h>

// Properties
GLuint screenWidth = 800, screenHeight = 600;

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void do_movement();
GLuint loadTexture(std::string path, GLboolean alpha = false);

// Camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

// The MAIN function, from here we start our application and run our Game loop
int main(int argc, char** argv) {
  bool grass = false;
  if (argc > 1 && std::string(argv[1]) != "0") {
    grass = true;
  }

  // Init GLFW
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "LearnOpenGL", nullptr, nullptr); // Windowed
  glfwMakeContextCurrent(window);

  // Set the required callback functions
  glfwSetKeyCallback(window, key_callback);
  glfwSetCursorPosCallback(window, mouse_callback);
  glfwSetScrollCallback(window, scroll_callback);

  // Options
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // Initialize GLEW to setup the OpenGL Function pointers
  glewExperimental = GL_TRUE;
  glewInit();

  // Define the viewport dimensions
  glViewport(0, 0, screenWidth, screenHeight);

  // Setup depth testing
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  if (grass == false) {
    // Enable blending
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  }

  // Setup and compile our shaders
  Shader *shader;
  try {
    std::vector<ShaderInfo> info;
    if (grass == true) {
      info = {
        ShaderInfo("grass.vert", GL_VERTEX_SHADER),
        ShaderInfo("grass.frag", GL_FRAGMENT_SHADER)
      };
    }
    else {
      info = {
        ShaderInfo("blending.vert", GL_VERTEX_SHADER),
        ShaderInfo("blending.frag", GL_FRAGMENT_SHADER)
      };
    }
    shader = new Shader(info, {"model", "view", "projection"});
  }
  catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
    glfwTerminate();
    return -1;
  }

  // Setup cube VAO
  auto    cubeVertices    = Cube::draw(1.0f, {"texcoords"});
  GLuint cubeVAO, cubeVBO;
  glGenVertexArrays(1, &cubeVAO);
  glGenBuffers(1, &cubeVBO);
  glBindVertexArray(cubeVAO);
  glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
  glBufferData(GL_ARRAY_BUFFER, cubeVertices.size() * sizeof(GLfloat),
               &cubeVertices[0], GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        5 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
                        5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
  glBindVertexArray(0);

  // Setup plane VAO
  GLfloat planeVertices[] = {
 // Positions             Texture Coords (note we set these higher than 1 that together with GL_REPEAT as texture wrapping mode will cause the floor texture to repeat)
    5.0f,  -0.5f,  5.0f,  2.0f, 0.0f,
    -5.0f, -0.5f,  5.0f,  0.0f, 0.0f,
    -5.0f, -0.5f, -5.0f,  0.0f, 2.0f,

    5.0f,  -0.5f,  5.0f,  2.0f, 0.0f,
    -5.0f, -0.5f, -5.0f,  0.0f, 2.0f,
    5.0f,  -0.5f, -5.0f,  2.0f, 2.0f								
  };
  GLuint planeVAO, planeVBO;
  glGenVertexArrays(1, &planeVAO);
  glGenBuffers(1, &planeVBO);
  glBindVertexArray(planeVAO);
  glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), &planeVertices, GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
  glBindVertexArray(0);

  // Setup transp
  std::vector<glm::vec3> transPos = {
    {-1.5f, 0.0f, -0.48f}, {1.5f, 0.0f, 0.51f}, {0.0f, 0.0f, 0.7f},
    {-0.3f, 0.0f, -2.3f}, {0.5f, 0.0f, -0.6f}
  };
  GLfloat transVertices[] = {
        // Positions         // Texture Coords (swapped y coordinates to flip texture)
        0.0f,  0.5f,  0.0f,  0.0f,  0.0f,
        0.0f, -0.5f,  0.0f,  0.0f,  1.0f,
        1.0f, -0.5f,  0.0f,  1.0f,  1.0f,

        0.0f,  0.5f,  0.0f,  0.0f,  0.0f,
        1.0f, -0.5f,  0.0f,  1.0f,  1.0f,
        1.0f,  0.5f,  0.0f,  1.0f,  0.0f
    };
  GLuint transVAO, transVBO;
  glGenVertexArrays(1, &transVAO);
  glGenBuffers(1, &transVBO);
  glBindVertexArray(transVAO);
  glBindBuffer(GL_ARRAY_BUFFER, transVBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(transVertices), &transVertices, GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
  glBindVertexArray(0);

  // Load textures
  GLuint cubeTexture  = loadTexture("../../resources/marble.jpg");
  GLuint floorTexture = loadTexture("../../resources/metal.png");
  GLuint transTexture = grass
                          ? loadTexture("../../resources/grass.png", true)
                          : loadTexture("../../resources/blending_transparent_window.png", true);

  // Draw in wireframe
  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  // Game loop
  while(!glfwWindowShouldClose(window)) {
    // Set frame time
    GLfloat currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    // Check and call events
    glfwPollEvents();
    do_movement();

    // Clear the colorbuffer
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glUseProgram(shader->id);   // <-- Don't forget this one!

    // Transformation matrices
    GLfloat aspect = ((GLfloat)screenWidth) / ((GLfloat)screenHeight);
    GLfloat scale = 1.05f;
    glm::mat4 model      = glm::mat4();
    glm::mat4 view       = camera.getView();
    glm::mat4 projection = camera.getProjection(0.0f, aspect, 0.2f, 100.0f);
    glUniformMatrix4fv(shader->uniforms["view"],       1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(shader->uniforms["projection"], 1, GL_FALSE, glm::value_ptr(projection));

    // Draw floor
    glBindVertexArray(planeVAO);
    glBindTexture(GL_TEXTURE_2D, floorTexture);
    glm::mat4 floor = glm::translate(glm::mat4(), glm::vec3(-1.0f, 0.0f, -1.0f));
    glUniformMatrix4fv(shader->uniforms["model"],
                       1, GL_FALSE, glm::value_ptr(floor));
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);

    // Draw the cubes
    glBindVertexArray(cubeVAO);
    glBindTexture(GL_TEXTURE_2D, cubeTexture);
    model = glm::translate(glm::mat4(), glm::vec3(-1.0f, 0.0f, -1.0f)); // Translate it down a bit so it's at the center of the scene
    glUniformMatrix4fv(shader->uniforms["model"],
                       1, GL_FALSE, glm::value_ptr(model));
    glDrawArrays(GL_TRIANGLES, 0, 36);
    model = glm::translate(glm::mat4(), glm::vec3(2.0f, 0.0f, 0.0f));
    glUniformMatrix4fv(shader->uniforms["model"],
                       1, GL_FALSE, glm::value_ptr(model));
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);

    // If rendering transparent textures, first sort them by distance from camera.
    std::map<GLfloat, glm::vec3> sorted;
    if (grass == false) {
      for (size_t i = 0; i < transPos.size(); i++) {
        GLfloat distance = glm::length(camera.getPosition() - transPos[i]);
        sorted[distance] = transPos[i];
      }
    }
    
    // Draw transparent quads
    glBindVertexArray(transVAO);
    glBindTexture(GL_TEXTURE_2D, transTexture);
    if (grass == true) {
      for (size_t i = 0; i < transPos.size(); i++) {
        model = glm::translate(glm::mat4(), transPos[i]);
        glUniformMatrix4fv(shader->uniforms["model"],
                           1, GL_FALSE, glm::value_ptr(model));
        glDrawArrays(GL_TRIANGLES, 0, 6);
      }
    }
    else {
      // Render transparent quads from furthest to nearest
      for (auto it = sorted.rbegin(); it != sorted.rend(); ++it) {
        model = glm::translate(glm::mat4(), it->second);
        glUniformMatrix4fv(shader->uniforms["model"],
                           1, GL_FALSE, glm::value_ptr(model));
        glDrawArrays(GL_TRIANGLES, 0, 6);
      }
    }
    glBindVertexArray(0);

    // Swap the buffers
    glfwSwapBuffers(window);
  }

  glDeleteVertexArrays(1, &cubeVAO);
  glDeleteVertexArrays(1, &planeVAO);
  glDeleteVertexArrays(1, &transVAO);
  glDeleteBuffers(1, &cubeVBO);
  glDeleteBuffers(1, &planeVBO);
  glDeleteBuffers(1, &transVBO);
  glDeleteProgram(shader->id);
  delete shader;
  glfwTerminate();

  return 0;
}

// Moves/alters the camera positions based on user input
void do_movement() {
  GLint side = 0, front = 0;

  if (keys[GLFW_KEY_W] || keys[GLFW_KEY_UP]) {
    front = front + 1;
  }
  if (keys[GLFW_KEY_S] || keys[GLFW_KEY_DOWN]) {
    front = front - 1;
  }
  if (keys[GLFW_KEY_A] || keys[GLFW_KEY_LEFT]) {
    side = side - 1;
  }
  if (keys[GLFW_KEY_D] || keys[GLFW_KEY_RIGHT]) {
    side = side + 1;
  }

  if (front != 0 || side != 0) {
    camera.move(side, front, deltaTime);
  }
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode) {
  if((key == GLFW_KEY_ESCAPE || key == GLFW_KEY_Q) && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);

  if(action == GLFW_PRESS)
    keys[key] = true;
  else if(action == GLFW_RELEASE)
    keys[key] = false;	
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
  if(firstMouse) {
    lastX = xpos;
    lastY = ypos;
    firstMouse = false;
  }

  GLfloat xoffset = xpos - lastX;
  GLfloat yoffset = lastY - ypos; 
  
  lastX = xpos;
  lastY = ypos;

  camera.rotate(xoffset, yoffset);
}	

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
  camera.zoom((GLfloat) yoffset);
}

GLuint loadTexture(std::string path, GLboolean alpha) {
  GLuint texture;
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  int width, height;
  unsigned char* image = SOIL_load_image(path.c_str(), &width, &height, 0,
                                         alpha ? SOIL_LOAD_RGBA : SOIL_LOAD_RGB);
  glTexImage2D(GL_TEXTURE_2D, 0, alpha ? GL_RGBA : GL_RGB,
               width, height, 0,
               alpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, image);
  glGenerateMipmap(GL_TEXTURE_2D);
  SOIL_free_image_data(image);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                  alpha ? GL_CLAMP_TO_EDGE : GL_REPEAT);	// Horizontal texture wrapping
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
                  alpha ? GL_CLAMP_TO_EDGE : GL_REPEAT);  // Vertical texture wrapping
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                  GL_LINEAR_MIPMAP_LINEAR);               // Texture minimizing
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                  GL_LINEAR);                             // Texture stretching
  glBindTexture(GL_TEXTURE_2D, 0); 

  return texture;
}
