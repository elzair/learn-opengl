#version 330 core

layout (location = 0) in vec3 position;

out VS_OUT {
  vec3 TexDir;
} vs_out;

layout (std140) uniform Matrices {
  mat4 projection;
  mat4 view;
};

void main() {
  vec4 pos      = projection * view * vec4(position, 1.0f);
  gl_Position   = pos.xyww; // Ensure cubemap always has depth of 1.0
  vs_out.TexDir = position;
}
