#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;

out VS_OUT {
 vec3 Normal;   // Adjust for nonuniform scaling
 vec3 Position; // Worldspace position vector
} vs_out;

layout (std140) uniform Matrices {
  mat4 projection;
  mat4 view;
};
uniform mat4 model;

void main() {
  gl_Position     = projection * view * model * vec4(position, 1.0f);
  vs_out.Normal   = mat3(transpose(inverse(model))) * normal;
  vs_out.Position = vec3(model * vec4(position, 1.0f));
}
