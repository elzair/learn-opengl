#version 330 core

in VS_OUT {
  vec3 TexDir;
} fs_in;

out     vec4        color;

uniform samplerCube cubemap;

void main() {
  color = texture(cubemap, fs_in.TexDir);
}
