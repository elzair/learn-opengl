#include <iostream>

// Std. Includes
#include <string>
#include <map>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// GL includes
#include "../../utils/camera.hpp"
#include "../../utils/cube.hpp"
#include "../../utils/shader.hpp"

// Math
#include "../../utils/glm/glm/glm.hpp"
#include "../../utils/glm/glm/gtc/matrix_transform.hpp"
#include "../../utils/glm/glm/gtc/type_ptr.hpp"

// Other Libs
#include <SOIL/SOIL.h>

struct Texture {
  GLuint name;
  GLuint unit;
};

// Needed for postprocessing
struct RenderTexture {
  GLuint id;
  GLuint texname;
  GLuint texunit;
  GLuint rbo;
  GLuint vao;
  GLuint vbo;
};

std::vector<std::vector<GLfloat>> kernels = {
  {
    -1.0, -1.0, -1.0,
    -1.0,  9.0, -1.0,
    -1.0, -1.0, -1.0
  },
  {
    1.0 / 16, 2.0 / 16, 1.0 / 16,
    2.0 / 16, 4.0 / 16, 2.0 / 16,
    1.0 / 16, 2.0 / 16, 1.0 / 16  
  },
  {
    1.0,  1.0, 1.0,
    1.0, -8.0, 1.0,
    1.0,  1.0, 1.0
  }
};

// Properties
GLuint screenWidth = 800, screenHeight = 600;

// Camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

GLuint  textures[GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS];
GLuint  texidx = 0;

// Function prototypes
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);
void mouseCallback(GLFWwindow* window, double xpos, double ypos);
void doMovement();
Texture loadTexture(std::string path, GLboolean alpha = false);
RenderTexture makeRenderTexture(GLuint width = screenWidth, GLuint height = screenHeight);
Texture loadCubemap(std::vector<std::string> texture_faces);

int main(int argc, char** argv) {
  
  int    kernelPos  = -1;
  GLuint filter     = 0;
  int    reflectArg = 0;
  bool   use_kernel = false;

  // Get reflect argument (if present)
  if (argc >= 2) { 
    std::string::size_type sz;

    reflectArg = std::stoi(std::string(argv[1]), &sz, 10);

    // Get filter argument (if present)
    if (argc >= 3) {
      int filterArg = std::stoi(std::string(argv[2]), &sz, 10);

      if (filterArg < 0) {
        std::cerr << "Filter argument must be positive!" << std::endl;
        return -1;
      }

      filter = (GLuint)filterArg;

      // Get kernel (if necessary)
      if (filter >= 4) {
        kernelPos  = filter - 4; // Kernels begin at option 4
        use_kernel = true;
      }
    }
  }
  
  /*
   * Window & OpenGL Initialization
   */
  
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "LearnOpenGL", nullptr, nullptr); // Windowed
  glfwMakeContextCurrent(window);

  // Set the required callback functions
  glfwSetKeyCallback(window, keyCallback);
  glfwSetCursorPosCallback(window, mouseCallback);
  glfwSetScrollCallback(window, scrollCallback);

  // Options
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // Initialize GLEW to setup the OpenGL Function pointers
  glewExperimental = GL_TRUE;
  glewInit();

  // Define the viewport dimensions
  glViewport(0, 0, screenWidth, screenHeight);

  // Setup depth testing
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  // Enable face culling
  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
  glFrontFace(GL_CCW);

  /*
   * Shader Initialization
   */

  // Setup and compile our shaders
  Shader* shader, *reflectShader, *refractShader, *cubemapShader, * quadShader, * kernelShader, * shader1p, * shader2p;
  try {
    shader = new Shader({
        ShaderInfo("primary.vert", GL_VERTEX_SHADER),
        ShaderInfo("primary.frag", GL_FRAGMENT_SHADER)
      },
      {"model", "diffuse_texture1"});
    reflectShader = new Shader({
        ShaderInfo("reflect.vert", GL_VERTEX_SHADER),
        ShaderInfo("reflect.frag", GL_FRAGMENT_SHADER)
      },
      {"model", "skybox", "viewPos"});
    refractShader = new Shader({
        ShaderInfo("refract.vert", GL_VERTEX_SHADER),
        ShaderInfo("refract.frag", GL_FRAGMENT_SHADER)
      },
      {"model", "skybox", "viewPos"});
    cubemapShader = new Shader({
        ShaderInfo("cubemap.vert", GL_VERTEX_SHADER),
        ShaderInfo("cubemap.frag", GL_FRAGMENT_SHADER)
      },
      {"cubemap"});
    quadShader = new Shader({
        ShaderInfo("quad.vert", GL_VERTEX_SHADER),
        ShaderInfo("quad.frag", GL_FRAGMENT_SHADER)
      },
      {"screenTexture", "filt"});
    kernelShader = new Shader({
        ShaderInfo("kernel.vert", GL_VERTEX_SHADER),
        ShaderInfo("kernel.frag", GL_FRAGMENT_SHADER)
      },
      {"screenTexture", "kernel"}
    );
  }
  catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
    glfwTerminate();
    return -1;
  }

  switch (reflectArg) {
  case 0:
    shader1p = shader;
    break;
  case 1:
    shader1p = reflectShader;
    break;
  case 2:
    shader1p = refractShader;
    break;
  default:
    std::cerr << "Invalid reflect argument: " << reflectArg << std::endl;
    glDeleteProgram(shader->id);
    glDeleteProgram(reflectShader->id);
    glDeleteProgram(refractShader->id);
    glDeleteProgram(cubemapShader->id);
    glDeleteProgram(quadShader->id);
    glDeleteProgram(kernelShader->id);
    delete shader;
    delete reflectShader;
    delete refractShader;
    delete cubemapShader;
    delete quadShader;
    delete kernelShader;
    glfwTerminate();
    return -2;
  }
  shader2p = use_kernel  == true ? kernelShader  : quadShader; // Use either regular postprocess shader or kernel based one

  /*
   * Vertex Array & Vertex Buffer Initialization
   */

  // Setup cube VAO
  std::vector<GLfloat> cube_vertices;
  GLsizei cube_vertices_stride;
  GLint sndattribsize;
  if (reflectArg > 0) {
    cube_vertices        = Cube::draw(1.0f, {"normals"});
    cube_vertices_stride = 6 * sizeof(GLfloat);
    sndattribsize        = 3;
  }
  else {
    cube_vertices        = Cube::draw(1.0f, {"texcoords"});
    cube_vertices_stride = 5 * sizeof(GLfloat);
    sndattribsize        = 2;
  };
  GLuint cubeVAO, cubeVBO;
  glGenVertexArrays(1, &cubeVAO);
  glGenBuffers(1, &cubeVBO);
  glBindVertexArray(cubeVAO);
  glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
  glBufferData(GL_ARRAY_BUFFER, cube_vertices.size() * sizeof(GLfloat),
               &cube_vertices[0], GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        cube_vertices_stride, (GLvoid*)0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, sndattribsize, GL_FLOAT, GL_FALSE,
                        cube_vertices_stride, (GLvoid*)(3 * sizeof(GLfloat)));
  glBindVertexArray(0);

  // Setup skybox VAO
  auto skyboxVertices = Cube::drawout(2.0f, {});
  GLuint skyboxVAO, skyboxVBO;
  glGenVertexArrays(1, &skyboxVAO);
  glGenBuffers(1, &skyboxVBO);
  glBindVertexArray(skyboxVAO);
  glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
  glBufferData(GL_ARRAY_BUFFER, skyboxVertices.size() * sizeof(GLfloat),
               &skyboxVertices[0], GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        3 * sizeof(GLfloat), (GLvoid*)0);
  glBindVertexArray(0);

  /*
   * Uniform Buffer Initialization
   */
  
  // First get relevant block indices
  GLuint shader1pidx = glGetUniformBlockIndex(shader1p->id,      "Matrices");
  GLuint cubemapidx  = glGetUniformBlockIndex(cubemapShader->id, "Matrices");
  // Then link each shader's uniform block to the same binding point
  glUniformBlockBinding(shader1p->id,      shader1pidx, 0);
  glUniformBlockBinding(cubemapShader->id, cubemapidx,  0);
  // Finally create the buffer
  GLuint viewAndProj;
  glGenBuffers(1, &viewAndProj);
  glBindBuffer(GL_UNIFORM_BUFFER, viewAndProj);
  glBufferData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4), NULL, GL_STATIC_DRAW);
  glBindBuffer(GL_UNIFORM_BUFFER, 0);
  // Define the range of the buffer that links to a uniform binding point
  glBindBufferRange(GL_UNIFORM_BUFFER, 0, viewAndProj, 0, 2 * sizeof(glm::mat4));

  /*
   * Framebuffer Initialization
   */

  RenderTexture rtex = makeRenderTexture();

  /*
   * Texture Initialization
   */
  
  Texture cube_texture    = loadTexture("../../resources/container.jpg");
  //Texture floorTexture   = loadTexture("../../resources/metal.png");
  Texture cubemap_texture = loadCubemap({
      "../../resources/skyboxes/default/right.jpg",
      "../../resources/skyboxes/default/left.jpg",
      "../../resources/skyboxes/default/top.jpg",
      "../../resources/skyboxes/default/bottom.jpg",
      "../../resources/skyboxes/default/back.jpg",
      "../../resources/skyboxes/default/front.jpg"
  });
  glActiveTexture(GL_TEXTURE0);

  // Draw in wireframe
  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  /*
   * Main Loop
   */ 

  while(!glfwWindowShouldClose(window)) {
    // Set frame time
    GLfloat currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    // Check and call events
    glfwPollEvents();
    doMovement();

    /*
     * 1st Pass: Render to framebuffer
     */

    glBindFramebuffer(GL_FRAMEBUFFER, rtex.id);

    // Clear the colorbuffer and enable depth testing
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    // Draw objects
    glUseProgram(shader1p->id);   // <-- Don't forget this one!

    // Transformation matrices
    GLfloat aspect = ((GLfloat)screenWidth) / ((GLfloat)screenHeight);
    glm::mat4 model      = glm::mat4();
    glm::vec3 viewPos    = camera.getPosition();
    glm::mat4 view       = camera.getView();
    glm::mat4 projection = camera.getProjection(0.0f, aspect, 0.2f, 100.0f);
    glBindBuffer(GL_UNIFORM_BUFFER, viewAndProj);
    glBufferSubData(GL_UNIFORM_BUFFER, 0,                 sizeof(glm::mat4), glm::value_ptr(projection));
    glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(view));
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    // Draw the cubes
    glBindVertexArray(cubeVAO);
    if (reflectArg > 0) {
      glUniform1i(shader1p->uniforms["skybox"], cubemap_texture.unit);
      glUniform3f(shader1p->uniforms["viewPos"], viewPos.x, viewPos.y, viewPos.z);
    }
    else {
      glUniform1i(shader1p->uniforms["diffuse_texture1"], cube_texture.unit);
    }
    model = glm::translate(glm::mat4(), glm::vec3(-1.0f, 0.0f, -1.0f)); // Translate it down a bit so it's at the center of the scene
    glUniformMatrix4fv(shader1p->uniforms["model"],
                       1, GL_FALSE, glm::value_ptr(model));
    glDrawArrays(GL_TRIANGLES, 0, 36);
    model = glm::translate(glm::mat4(), glm::vec3(2.0f, 0.0f, 0.0f));
    glUniformMatrix4fv(shader1p->uniforms["model"],
                       1, GL_FALSE, glm::value_ptr(model));
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);

    // Render skybox last
    glDepthFunc(GL_LEQUAL);
    glUseProgram(cubemapShader->id);
    glm::mat4 skyboxView = glm::mat4(glm::mat3(view));
    glBindBuffer(GL_UNIFORM_BUFFER, viewAndProj);
    glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(skyboxView));
    //glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindVertexArray(skyboxVAO);
    glUniform1i(cubemapShader->uniforms["cubemap"], cubemap_texture.unit);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);
    glDepthFunc(GL_LESS);

    /*
     * 2nd Pass: Render framebuffer
     */ 

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(shader2p->id);
    glBindVertexArray(rtex.vao);
    glDisable(GL_DEPTH_TEST);

    if (use_kernel == true) {
      glUniform1fv(shader2p->uniforms["kernel"], 9, &(kernels[kernelPos])[0]);
    }
    else {
      glUniform1i(shader2p->uniforms["filt"], filter);
    }
    glUniform1i(shader2p->uniforms["screenTexture"], rtex.texunit);

    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);

    // Swap the buffers
    glfwSwapBuffers(window);
  }

  // Free resources
  glDeleteVertexArrays(1, &cubeVAO);
  glDeleteVertexArrays(1, &rtex.vao);
  glDeleteVertexArrays(1, &skyboxVAO);
  glDeleteBuffers(1, &cubeVBO);
  glDeleteBuffers(1, &rtex.vbo);
  glDeleteBuffers(1, &skyboxVBO);
  glDeleteBuffers(1, &viewAndProj);
  glDeleteFramebuffers(1, &rtex.id);
  glDeleteProgram(shader->id);
  glDeleteProgram(reflectShader->id);
  glDeleteProgram(refractShader->id);
  glDeleteProgram(cubemapShader->id);
  glDeleteProgram(quadShader->id);
  glDeleteProgram(kernelShader->id);
  delete shader;
  delete reflectShader;
  delete refractShader;
  delete cubemapShader;
  delete quadShader;
  delete kernelShader;
  glfwTerminate();

  return 0;
}

// Moves/alters the camera positions based on user input
void doMovement() {
  GLint side = 0, front = 0;

  if (keys[GLFW_KEY_W] || keys[GLFW_KEY_UP]) {
    front = front + 1;
  }
  if (keys[GLFW_KEY_S] || keys[GLFW_KEY_DOWN]) {
    front = front - 1;
  }
  if (keys[GLFW_KEY_A] || keys[GLFW_KEY_LEFT]) {
    side = side - 1;
  }
  if (keys[GLFW_KEY_D] || keys[GLFW_KEY_RIGHT]) {
    side = side + 1;
  }

  if (front != 0 || side != 0) {
    camera.move(side, front, deltaTime);
  }
}

// Is called whenever a key is pressed/released via GLFW
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode) {
  if((key == GLFW_KEY_ESCAPE || key == GLFW_KEY_Q) && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);

  if(action == GLFW_PRESS)
    keys[key] = true;
  else if(action == GLFW_RELEASE)
    keys[key] = false;	
}

void mouseCallback(GLFWwindow* window, double xpos, double ypos) {
  if(firstMouse) {
    lastX = xpos;
    lastY = ypos;
    firstMouse = false;
  }

  GLfloat xoffset = xpos - lastX;
  GLfloat yoffset = lastY - ypos; 
  
  lastX = xpos;
  lastY = ypos;

  camera.rotate(xoffset, yoffset);
}	

void scrollCallback(GLFWwindow* window, double xoffset, double yoffset) {
  camera.zoom((GLfloat) yoffset);
}

Texture loadTexture(std::string path, GLboolean alpha) {
  Texture texture;
  glGenTextures(1, &texture.name);
  texture.unit = texidx++;
  glActiveTexture(GL_TEXTURE0+texture.unit);
  glBindTexture(GL_TEXTURE_2D, texture.name);
  int width, height;
  unsigned char* image = SOIL_load_image(path.c_str(), &width, &height, 0,
                                         alpha ? SOIL_LOAD_RGBA : SOIL_LOAD_RGB);
  glTexImage2D(GL_TEXTURE_2D, 0, alpha ? GL_RGBA : GL_RGB,
               width, height, 0,
               alpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, image);
  glGenerateMipmap(GL_TEXTURE_2D);
  SOIL_free_image_data(image);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                  alpha ? GL_CLAMP_TO_EDGE : GL_REPEAT);	// Horizontal texture wrapping
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
                  alpha ? GL_CLAMP_TO_EDGE : GL_REPEAT);  // Vertical texture wrapping
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                  GL_LINEAR_MIPMAP_LINEAR);               // Texture minimizing
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                  GL_LINEAR);                             // Texture stretching

  return texture;
}

RenderTexture makeRenderTexture(GLuint width, GLuint height) {
  // Create and bind framebuffer
  RenderTexture buf;
  glGenFramebuffers(1, &buf.id);
  glBindFramebuffer(GL_FRAMEBUFFER, buf.id);
  
  // Create blank texture that spans entire screen
  glGenTextures(1, &buf.texname);
  buf.texunit = texidx++;
  glActiveTexture(GL_TEXTURE0+buf.texunit);
  glBindTexture(GL_TEXTURE_2D, buf.texname);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height,
               0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  // Attach texture to framebuffer
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                         GL_TEXTURE_2D, buf.texname, 0);

  // Create renderbuffer
  glGenRenderbuffers(1, &buf.rbo);
  glBindRenderbuffer(GL_RENDERBUFFER, buf.rbo); 
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
  glBindRenderbuffer(GL_RENDERBUFFER, 0);
  
  // Attach renderbuffer to depth & stencil attachments
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT,
                            GL_RENDERBUFFER, buf.rbo);

  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    throw std::runtime_error("Cannot complete Framebuffer!");
  }
  
  glBindFramebuffer(GL_FRAMEBUFFER, 0);  // Unbind framebuffer
  
  // Create quad for rendering to texture
  GLfloat quadVertices[] = {
    // Positions         // Texture Coords (swapped y coordinates to flip texture)
    -1.0f,  1.0f, 0.0f,  0.0f,  1.0f,
    -1.0f, -1.0f, 0.0f,  0.0f,  0.0f,
     1.0f, -1.0f, 0.0f,  1.0f,  0.0f,

    -1.0f,  1.0f, 0.0f,  0.0f,  1.0f,
     1.0f, -1.0f, 0.0f,  1.0f,  0.0f,
     1.0f,  1.0f, 0.0f,  1.0f,  1.0f
  };

  glGenVertexArrays(1, &buf.vao);
  glGenBuffers(1, &buf.vbo);
  glBindVertexArray(buf.vao);
  glBindBuffer(GL_ARRAY_BUFFER, buf.vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
  glBindVertexArray(0);

  return buf;
}

Texture loadCubemap(std::vector<std::string> texture_faces) {
  if (texture_faces.size() != 6) {
    throw std::runtime_error("Cubemaps need six textures!");
  }
  
  // Create texture
  Texture texture;
  glGenTextures(1, &texture.name);
  texture.unit = texidx++;
  glActiveTexture(GL_TEXTURE0+texture.unit);
  glBindTexture(GL_TEXTURE_CUBE_MAP, texture.name);

  // Load textures for all six faces of cube (x is some constant)
  // x+0 GL_TEXTURE_CUBE_MAP_POSITIVE_X 	Right
  // x+1 GL_TEXTURE_CUBE_MAP_NEGATIVE_X 	Left
  // x+2 GL_TEXTURE_CUBE_MAP_POSITIVE_Y 	Top
  // x+3 GL_TEXTURE_CUBE_MAP_NEGATIVE_Y 	Bottom
  // x+4 GL_TEXTURE_CUBE_MAP_POSITIVE_Z 	Back
  // x+5 GL_TEXTURE_CUBE_MAP_NEGATIVE_Z 	Front
  int width, height;
  uint8_t* image;
  for (size_t i = 0; i < texture_faces.size(); i++) {
    image = (uint8_t*)SOIL_load_image(texture_faces[i].c_str(), &width, &height, 0, SOIL_LOAD_RGB);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
                 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image
    );
    SOIL_free_image_data(image);
  }

  // Bind texture parameters
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);  

  return texture;
}
