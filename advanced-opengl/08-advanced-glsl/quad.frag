#version 330 core

in VS_OUT {
 vec2 TexCoords;
} fs_in;

out     vec4      color;

uniform sampler2D screenTexture;
uniform int       filt;

void main() { 
  float average = 0.0f;
  
  switch(filt) {
  case 0: // No filter
    color = texture(screenTexture, fs_in.TexCoords);
    break;
  case 1: // Inversion
    color = vec4(vec3(1.0 - texture(screenTexture, fs_in.TexCoords)), 1.0f); // Inversion
    break;
  case 2: // Grayscale
    color         = texture(screenTexture, fs_in.TexCoords);
    average = (color.r + color.g + color.b) / 3.0f;
    color         = vec4(average, average, average, 1.0f);
    break;
  case 3: // Improved Grayscale
    color         = texture(screenTexture, fs_in.TexCoords);
    average = 0.2126 * color.r + 0.7152 * color.g + 0.0722 * color.b;
    color         = vec4(average, average, average, 1.0f);
    break;
  }
}
