#version 330 core

in VS_OUT {
 vec3 Normal;   // Adjust for nonuniform scaling
 vec3 Position; // Worldspace position vector
} fs_in;

out     vec4        color;

uniform vec3        viewPos;
uniform samplerCube skybox;

void main() {
  float ratio = 1.00 / 1.52;  // Ratio of refractive indices of air and glass
  vec3  I     = normalize(fs_in.Position - viewPos);
  vec3  R     = refract(I, normalize(fs_in.Normal), ratio);
  color       = texture(skybox, R);
}
