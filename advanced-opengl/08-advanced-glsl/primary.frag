#version 330 core

in VS_OUT {
 vec2 TexCoords;
} fs_in;

out     vec4      color;

uniform sampler2D diffuse_texture1;

void main() {
  color = texture(diffuse_texture1, fs_in.TexCoords);
}
