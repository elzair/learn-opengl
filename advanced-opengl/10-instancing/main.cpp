#include <iostream>

// Std. Includes
#include <string>
#include <map>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// GL includes
#include "../../utils/camera.hpp"
#include "../../utils/cube.hpp"
#include "../../utils/shader.hpp"

// Math
#include <cmath>
#include "../../utils/glm/glm/glm.hpp"
#include "../../utils/glm/glm/gtc/matrix_transform.hpp"
#include "../../utils/glm/glm/gtc/type_ptr.hpp"

// Other Libs
#include <SOIL/SOIL.h>

// Model loading
#include "../../utils/model.hpp"

std::vector<GLfloat> quad_vertices = {
  // Positions     // Colors
  -0.05f,  0.05f,  1.0f, 0.0f, 0.0f,
   0.05f, -0.05f,  0.0f, 1.0f, 0.0f,
  -0.05f, -0.05f,  0.0f, 0.0f, 1.0f,

  -0.05f,  0.05f,  1.0f, 0.0f, 0.0f,
   0.05f, -0.05f,  0.0f, 1.0f, 0.0f,   
   0.05f,  0.05f,  0.0f, 1.0f, 1.0f		    		
};

// Properties
GLuint screenWidth = 800, screenHeight = 600;

// Camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

// Function prototypes
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);
void mouseCallback(GLFWwindow* window, double xpos, double ypos);
void doMovement();

int main(int argc, char** argv) {
  int instance_type = 0;
  if (argc > 1) {
    std::string::size_type sz;
    instance_type = std::stoi(std::string(argv[1]), &sz, 10);
  }
  
  /*
   * Window & OpenGL Initialization
   */
  
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "LearnOpenGL", nullptr, nullptr); // Windowed
  glfwMakeContextCurrent(window);

  // Set the required callback functions
  glfwSetKeyCallback(window, keyCallback);
  glfwSetCursorPosCallback(window, mouseCallback);
  glfwSetScrollCallback(window, scrollCallback);

  // Options
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // Initialize GLEW to setup the OpenGL Function quaders
  glewExperimental = GL_TRUE;
  glewInit();

  // Define the viewport dimensions
  glViewport(0, 0, screenWidth, screenHeight);

  //glEnable(GL_DEPTH_TEST);

  /*
   * Shader Initialization
   */

  // Setup and compile our shaders
  std::vector<std::string> initial_shader_uniforms;
  if (instance_type == 0) {
    for (size_t i = 0; i < 100; i++) {
      initial_shader_uniforms.emplace_back("offsets[" + std::to_string(i) +"]");
    }
  }

  Shader* initial_shader, * instance_shader, * scale_shader, * model_noninstanced_shader, * model_instanced_shader, * shaderp;
  try {
    initial_shader = new Shader({
        ShaderInfo("initial.vert", GL_VERTEX_SHADER),
        ShaderInfo("initial.frag", GL_FRAGMENT_SHADER)
      },
      initial_shader_uniforms);
    instance_shader = new Shader({
        ShaderInfo("instance.vert", GL_VERTEX_SHADER),
        ShaderInfo("instance.frag", GL_FRAGMENT_SHADER)
      },
      {});
    scale_shader = new Shader({
        ShaderInfo("scale.vert", GL_VERTEX_SHADER),
        ShaderInfo("scale.frag", GL_FRAGMENT_SHADER)
      },
      {});
    model_noninstanced_shader = new Shader({
        ShaderInfo("model-noninstanced.vert", GL_VERTEX_SHADER),
        ShaderInfo("model-noninstanced.frag", GL_FRAGMENT_SHADER)
      },
      {"projection", "view", "model", "material.texture_diffuse1"});
    model_instanced_shader = new Shader({
        ShaderInfo("model-instanced.vert", GL_VERTEX_SHADER),
        ShaderInfo("model-instanced.frag", GL_FRAGMENT_SHADER)
      },
      {"projection", "view", "material.texture_diffuse1"});
  }
  catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
    glfwTerminate();
    return -1;
  }

  switch(instance_type) {
  case 0:
    shaderp = initial_shader;
    break;
  case 1:
    shaderp = instance_shader;
    break;
  case 2:
    shaderp = scale_shader;
    break;
  case 3:
    shaderp = model_noninstanced_shader;
    break;
  case 4:
    shaderp = model_noninstanced_shader;
    break;
  }

  /*
   * Initialize Quad Offsets
   */

  glm::vec2 translations[100];
  int index = 0;
  GLfloat offset = 0.1f;

  if (instance_type < 3) {
    for(GLint y = -10; y < 10; y += 2) {
        for(GLint x = -10; x < 10; x += 2) {
            glm::vec2 translation;
            translation.x = (GLfloat)x / 10.0f + offset;
            translation.y = (GLfloat)y / 10.0f + offset;
            translations[index++] = translation;
        }
    }  

    // Initialize uniform variables (for initial shader only)
    if (instance_type == 0) {
      glUseProgram(shaderp->id);
      for (size_t i = 0; i < 100; i++) {
        glUniform2f(shaderp->uniforms["offsets[" + std::to_string(i) + "]"], translations[i].x, translations[i].y);
      }
    }
  }

  /*
   * Initialize Asteroid Data
   */

  GLuint amount = 1000;
  glm::mat4* modelMatrices;
  modelMatrices = new glm::mat4[amount];
  srand(glfwGetTime()); // initialize random seed	
  GLfloat radius = 50.0;
  GLfloat asteroid_offset = 2.5f;

  if (instance_type >= 3) {
    for(GLuint i = 0; i < amount; i++) {
      glm::mat4 model;
      // 1. Translation: displace along circle with 'radius' in range [-asteroid_offset, asteroid_offset]
      GLfloat angle = (GLfloat)i / (GLfloat)amount * 360.0f;
      GLfloat displacement = (rand() % (GLint)(2 * asteroid_offset * 100)) / 100.0f - asteroid_offset;
      GLfloat x = sin(angle) * radius + displacement;
      displacement = (rand() % (GLint)(2 * asteroid_offset * 100)) / 100.0f - asteroid_offset;
      GLfloat y = displacement * 0.4f; // y value has smaller displacement
      displacement = (rand() % (GLint)(2 * asteroid_offset * 100)) / 100.0f - asteroid_offset;
      GLfloat z = cos(angle) * radius + displacement;
      model = glm::translate(model, glm::vec3(x, y, z));        
      // 2. Scale: Scale between 0.05 and 0.25f
      GLfloat scale = (rand() % 20) / 100.0f + 0.05;
      model = glm::scale(model, glm::vec3(scale));		        
      // 3. Rotation: add random rotation around a (semi)randomly picked rotation axis vector
      GLfloat rotAngle = (rand() % 360);
      model = glm::rotate(model, rotAngle, glm::vec3(0.4f, 0.6f, 0.8f));
      // 4. Now add to list of matrices
      modelMatrices[i] = model;
    }  
  }

  /*
   * Vertex Array & Vertex Buffer Initialization
   */

  GLuint quadVAO, quadVBO, instanceVBO;
  if (instance_type < 3) {
    glGenVertexArrays(1, &quadVAO);
    glGenBuffers(1, &quadVBO);
    glBindVertexArray(quadVAO);
    glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
    glBufferData(GL_ARRAY_BUFFER, quad_vertices.size() * sizeof(GLfloat),
                 &quad_vertices[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    if (instance_type == 1 || instance_type == 2) {
      glGenBuffers(1, &instanceVBO);
      glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
      glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * 100, &translations[0], GL_STATIC_DRAW);
      glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
                          5 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
                          5 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));
    if (instance_type == 1 || instance_type == 2) {
      glEnableVertexAttribArray(2);
      glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
      glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*)0);
    }
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glVertexAttribDivisor(2, 1);
    glBindVertexArray(0);
  }

  /*
   * Model Initialization
   */

  Model planet("../../resources/models/planet/planet.obj");
  Model asteroid("../../resources/models/rock/rock.obj");
  std::vector<GLuint> instance_matrix_bufs;

  for (GLuint i = 0; i < asteroid.meshes.size(); i++) {
    // Add new VBO
    GLuint VAO = asteroid.meshes[i].VAO;
    GLuint buffer;
    glBindVertexArray(VAO);
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, amount * sizeof(glm::mat4),
                 &modelMatrices[0], GL_STATIC_DRAW);
    // Enable new Vertex Attributes
    size_t vec4Size = sizeof(glm::vec4);
    glEnableVertexAttribArray(3); 
    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE,
                          4 * vec4Size, (GLvoid*)0);
    glEnableVertexAttribArray(4); 
    glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE,
                          4 * vec4Size, (GLvoid*)(vec4Size));
    glEnableVertexAttribArray(5); 
    glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE,
                          4 * vec4Size, (GLvoid*)(2 * vec4Size));
    glEnableVertexAttribArray(6); 
    glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE,
                          4 * vec4Size, (GLvoid*)(3 * vec4Size));

    glVertexAttribDivisor(3, 1);
    glVertexAttribDivisor(4, 1);
    glVertexAttribDivisor(5, 1);
    glVertexAttribDivisor(6, 1);
    instance_matrix_bufs.emplace_back(buffer);

    glBindVertexArray(0);
  }

  /*
   * Main Loop
   */ 

  if (instance_type >= 3) {
    glEnable(GL_DEPTH_TEST);
    camera.setPosition(glm::vec3(0.0f, 0.0f, 155.0f));
  }

  while(!glfwWindowShouldClose(window)) {
    // Set frame time
    GLfloat currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    // Check and call events
    glfwPollEvents();
    doMovement();

    // Clear the colorbuffer and enable depth testing
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    if (instance_type < 3) {
      glClear(GL_COLOR_BUFFER_BIT);
    }
    else if (instance_type >= 3) {
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    // Draw objects 
    glUseProgram(shaderp->id);   // <-- Don't forget this one!

    // Transformation matricese
    GLfloat aspect       = ((GLfloat)screenWidth) / ((GLfloat)screenHeight);
    glm::mat4 projection = camera.getProjection(0.0f, aspect, 1.0f, 1000.0f);
    glm::mat4 view       = camera.getView();

    if (instance_type < 3) { // Draw Quads
      glBindVertexArray(quadVAO);
      glDrawArraysInstanced(GL_TRIANGLES, 0, 6, 100);
      glBindVertexArray(0);
    }
    else if (instance_type == 3) {                   // Draw Planet & Asteroids
      //glm::mat4 model = glm::mat4();
      glm::mat4 model = glm::translate(glm::mat4(), glm::vec3(0.0f, -5.0f, 0.0f));
      glUniformMatrix4fv(shaderp->uniforms["projection"], 1, GL_FALSE, glm::value_ptr(projection));
      glUniformMatrix4fv(shaderp->uniforms["view"],       1, GL_FALSE, glm::value_ptr(view));
      model           = glm::scale(model, glm::vec3(4.0f, 4.0f, 4.0f));
      glUniformMatrix4fv(shaderp->uniforms["model"], 1, GL_FALSE, glm::value_ptr(model));
      planet.Draw(shaderp);

      for (GLuint i = 0; i < amount; i++) {
        glUniformMatrix4fv(shaderp->uniforms["model"], 1, GL_FALSE, glm::value_ptr(modelMatrices[i]));
        asteroid.Draw(shaderp);
      }
    }
    else if (instance_type == 4) {
      // First draw planet
      glUniformMatrix4fv(shaderp->uniforms["projection"], 1, GL_FALSE, glm::value_ptr(projection));
      glUniformMatrix4fv(shaderp->uniforms["view"],       1, GL_FALSE, glm::value_ptr(view));
      glm::mat4 model = glm::translate(glm::mat4(), glm::vec3(0.0f, -5.0f, 0.0f));
      model           = glm::scale(model, glm::vec3(4.0f, 4.0f, 4.0f));
      glUniformMatrix4fv(shaderp->uniforms["model"], 1, GL_FALSE, glm::value_ptr(model));
      planet.Draw(shaderp);

      // Then draw asteroids
      glUseProgram(model_instanced_shader->id);
      glUniformMatrix4fv(model_instanced_shader->uniforms["projection"], 1, GL_FALSE, glm::value_ptr(projection));
      glUniformMatrix4fv(model_instanced_shader->uniforms["view"],       1, GL_FALSE, glm::value_ptr(view));
      glBindTexture(GL_TEXTURE_2D, asteroid.textures_loaded[0].id); // Note we also made the textures_loaded vector public (instead of private) from the model class.
      for (GLuint i = 0; i < amount; i++) {
        glBindVertexArray(asteroid.meshes[i].VAO);
        glDrawElementsInstanced(GL_TRIANGLES, asteroid.meshes[i].vertices.size(),
                                GL_UNSIGNED_INT, 0, amount);
        glBindVertexArray(0);
      }
    }

    // Swap the buffers
    glfwSwapBuffers(window);
  }

  // Free resources
  if (instance_type < 3) {
    glDeleteVertexArrays(1, &quadVAO);
    glDeleteBuffers(1, &quadVBO);
    glDeleteBuffers(1, &instanceVBO);
  }
  else if (instance_type == 4) {
    glDeleteBuffers(instance_matrix_bufs.size(), &instance_matrix_bufs[0]);
  }
  glDeleteProgram(initial_shader->id);
  glDeleteProgram(instance_shader->id);
  glDeleteProgram(scale_shader->id);
  glDeleteProgram(model_noninstanced_shader->id);
  glDeleteProgram(model_instanced_shader->id);
  delete initial_shader;
  delete instance_shader;
  delete scale_shader;
  delete model_noninstanced_shader;
  delete model_instanced_shader;
  glfwTerminate();

  return 0;
}

// Moves/alters the camera positions based on user input
void doMovement() {
  GLint side = 0, front = 0;

  if (keys[GLFW_KEY_W] || keys[GLFW_KEY_UP]) {
    front = front + 1;
  }
  if (keys[GLFW_KEY_S] || keys[GLFW_KEY_DOWN]) {
    front = front - 1;
  }
  if (keys[GLFW_KEY_A] || keys[GLFW_KEY_LEFT]) {
    side = side - 1;
  }
  if (keys[GLFW_KEY_D] || keys[GLFW_KEY_RIGHT]) {
    side = side + 1;
  }

  if (front != 0 || side != 0) {
    camera.move(side, front, deltaTime);
  }
}

// Is called whenever a key is pressed/released via GLFW
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode) {
  if((key == GLFW_KEY_ESCAPE || key == GLFW_KEY_Q) && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);

  if(action == GLFW_PRESS)
    keys[key] = true;
  else if(action == GLFW_RELEASE)
    keys[key] = false;	
}

void mouseCallback(GLFWwindow* window, double xpos, double ypos) {
  if(firstMouse) {
    lastX = xpos;
    lastY = ypos;
    firstMouse = false;
  }

  GLfloat xoffset = xpos - lastX;
  GLfloat yoffset = lastY - ypos; 
  
  lastX = xpos;
  lastY = ypos;

  camera.rotate(xoffset, yoffset);
}	

void scrollCallback(GLFWwindow* window, double xoffset, double yoffset) {
  camera.zoom((GLfloat) yoffset);
}
