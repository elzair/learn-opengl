#include <iostream>

// Std. Includes
#include <string>
#include <map>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// GL includes
#include "../../utils/camera.hpp"
#include "../../utils/cube.hpp"
#include "../../utils/shader.hpp"

// Math
#include "../../utils/glm/glm/glm.hpp"
#include "../../utils/glm/glm/gtc/matrix_transform.hpp"
#include "../../utils/glm/glm/gtc/type_ptr.hpp"

// Other Libs
#include <SOIL/SOIL.h>

// Needed for postprocessing
struct RenderTexture {
  GLuint id;
  GLuint texid;
  GLuint rbo;
  GLuint vao;
  GLuint vbo;
};

std::vector<std::vector<GLfloat>> kernels = {
  {
    -1.0, -1.0, -1.0,
    -1.0,  9.0, -1.0,
    -1.0, -1.0, -1.0
  },
  {
    1.0 / 16, 2.0 / 16, 1.0 / 16,
    2.0 / 16, 4.0 / 16, 2.0 / 16,
    1.0 / 16, 2.0 / 16, 1.0 / 16  
  },
  {
    1.0,  1.0, 1.0,
    1.0, -8.0, 1.0,
    1.0,  1.0, 1.0
  }
};

// Properties
GLuint screenWidth = 800, screenHeight = 600;

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void do_movement();
GLuint loadTexture(std::string path, GLboolean alpha = false);
RenderTexture makeRenderTexture(GLuint width = screenWidth, GLuint height = screenHeight);

// Camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

int main(int argc, char** argv) {
  int    kernelPos  = -1;
  GLuint filter     = 0;
  bool   use_kernel = false;
  if (argc > 1) {
    std::string::size_type sz;
    filter = (GLuint)std::stoi(std::string(argv[1]), &sz, 10);

    if (filter >= 4) {
      kernelPos  = filter - 4; // Kernels begin at option 4
      use_kernel = true;
    }
  }
  
  /*
   * Window & OpenGL Initialization
   */
  
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "LearnOpenGL", nullptr, nullptr); // Windowed
  glfwMakeContextCurrent(window);

  // Set the required callback functions
  glfwSetKeyCallback(window, key_callback);
  glfwSetCursorPosCallback(window, mouse_callback);
  glfwSetScrollCallback(window, scroll_callback);

  // Options
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // Initialize GLEW to setup the OpenGL Function pointers
  glewExperimental = GL_TRUE;
  glewInit();

  // Define the viewport dimensions
  glViewport(0, 0, screenWidth, screenHeight);

  // Setup depth testing
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  //// Enable blending
  //glEnable(GL_BLEND);
  //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Enable face culling
  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
  glFrontFace(GL_CCW);

  /*
   * Shader Initialization
   */

  // Setup and compile our shaders
  Shader* shader, * quadShader, * kernelShader, * shaderp;
  try {
    shader = new Shader({
        ShaderInfo("fb.vert", GL_VERTEX_SHADER),
        ShaderInfo("fb.frag", GL_FRAGMENT_SHADER)
      },
      {"model", "view", "projection", "texture1"});
    quadShader = new Shader({
        ShaderInfo("quad.vert", GL_VERTEX_SHADER),
        ShaderInfo("quad.frag", GL_FRAGMENT_SHADER)
      },
      {"screenTexture", "filt"});
    kernelShader = new Shader({
        ShaderInfo("kernel.vert", GL_VERTEX_SHADER),
        ShaderInfo("kernel.frag", GL_FRAGMENT_SHADER)
      },
      {"screenTexture", "kernel"}
    );
  }
  catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
    glfwTerminate();
    return -1;
  }

  shaderp = use_kernel == true ? kernelShader : quadShader; // Use either regular postprocess shader or kernel based one

  /*
   * Vertex Array & Vertex Buffer Initialization
   */

  // Setup cube VAO
  auto    cubeVertices = Cube::draw(1.0f, {"texcoords"});
  GLuint cubeVAO, cubeVBO;
  glGenVertexArrays(1, &cubeVAO);
  glGenBuffers(1, &cubeVBO);
  glBindVertexArray(cubeVAO);
  glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
  glBufferData(GL_ARRAY_BUFFER, cubeVertices.size() * sizeof(GLfloat),
               &cubeVertices[0], GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        5 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
                        5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
  glBindVertexArray(0);

  // Setup plane VAO
  GLfloat planeVertices[] = {
 // Positions             Texture Coords (note we set these higher than 1 that together with GL_REPEAT as texture wrapping mode will cause the floor texture to repeat)
    5.0f,  -0.5f,  5.0f,  2.0f, 0.0f,
    -5.0f, -0.5f, -5.0f,  0.0f, 2.0f,
    -5.0f, -0.5f,  5.0f,  0.0f, 0.0f,

    5.0f,  -0.5f,  5.0f,  2.0f, 0.0f,
    5.0f,  -0.5f, -5.0f,  2.0f, 2.0f,								
    -5.0f, -0.5f, -5.0f,  0.0f, 2.0f
  };
  GLuint planeVAO, planeVBO;
  glGenVertexArrays(1, &planeVAO);
  glGenBuffers(1, &planeVBO);
  glBindVertexArray(planeVAO);
  glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), &planeVertices, GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
  glBindVertexArray(0);

  /*
   * Framebuffer Initialization
   */

  RenderTexture rtex = makeRenderTexture();

  /*
   * Texture Initialization
   */
  
  GLuint cubeTexture  = loadTexture("../../resources/container.jpg");
  GLuint floorTexture = loadTexture("../../resources/metal.png");

  // Draw in wireframe
  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  /*
   * Main Loop
   */ 

  while(!glfwWindowShouldClose(window)) {
    // Set frame time
    GLfloat currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    // Check and call events
    glfwPollEvents();
    do_movement();

    /*
     * 1st Pass: Render to framebuffer
     */

    glBindFramebuffer(GL_FRAMEBUFFER, rtex.id);

    // Clear the colorbuffer and enable depth testing
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);

    glUseProgram(shader->id);   // <-- Don't forget this one!

    // Transformation matrices
    GLfloat aspect = ((GLfloat)screenWidth) / ((GLfloat)screenHeight);
    glm::mat4 model      = glm::mat4();
    glm::mat4 view       = camera.getView();
    glm::mat4 projection = camera.getProjection(0.0f, aspect, 0.2f, 100.0f);
    glUniformMatrix4fv(shader->uniforms["view"],       1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(shader->uniforms["projection"], 1, GL_FALSE, glm::value_ptr(projection));

    // Draw floor
    glBindVertexArray(planeVAO);
    glBindTexture(GL_TEXTURE_2D, floorTexture);
    glm::mat4 floor = glm::translate(glm::mat4(), glm::vec3(-1.0f, 0.0f, -1.0f));
    glUniformMatrix4fv(shader->uniforms["model"],
                       1, GL_FALSE, glm::value_ptr(floor));
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);

    // Draw the cubes
    glBindVertexArray(cubeVAO);
    glBindTexture(GL_TEXTURE_2D, cubeTexture);
    model = glm::translate(glm::mat4(), glm::vec3(-1.0f, 0.0f, -1.0f)); // Translate it down a bit so it's at the center of the scene
    glUniformMatrix4fv(shader->uniforms["model"],
                       1, GL_FALSE, glm::value_ptr(model));
    glDrawArrays(GL_TRIANGLES, 0, 36);
    model = glm::translate(glm::mat4(), glm::vec3(2.0f, 0.0f, 0.0f));
    glUniformMatrix4fv(shader->uniforms["model"],
                       1, GL_FALSE, glm::value_ptr(model));
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);

    /*
     * 2nd Pass: Render framebuffer
     */ 

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(shaderp->id);
    glBindVertexArray(rtex.vao);
    glDisable(GL_DEPTH_TEST);
    glBindTexture(GL_TEXTURE_2D, rtex.texid);

    if (use_kernel == true) {
      glUniform1fv(shaderp->uniforms["kernel"], 9, &(kernels[kernelPos])[0]);
    }
    else {
      glUniform1i(shaderp->uniforms["filt"], filter);
    }

    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);

    // Swap the buffers
    glfwSwapBuffers(window);
  }

  glDeleteVertexArrays(1, &cubeVAO);
  glDeleteVertexArrays(1, &planeVAO);
  glDeleteVertexArrays(1, &rtex.vao);
  glDeleteBuffers(1, &cubeVBO);
  glDeleteBuffers(1, &planeVBO);
  glDeleteBuffers(1, &rtex.vbo);
  glDeleteFramebuffers(1, &rtex.id);
  glDeleteProgram(shader->id);
  glDeleteProgram(quadShader->id);
  glDeleteProgram(kernelShader->id);
  delete shader;
  delete quadShader;
  delete kernelShader;
  glfwTerminate();

  return 0;
}

// Moves/alters the camera positions based on user input
void do_movement() {
  GLint side = 0, front = 0;

  if (keys[GLFW_KEY_W] || keys[GLFW_KEY_UP]) {
    front = front + 1;
  }
  if (keys[GLFW_KEY_S] || keys[GLFW_KEY_DOWN]) {
    front = front - 1;
  }
  if (keys[GLFW_KEY_A] || keys[GLFW_KEY_LEFT]) {
    side = side - 1;
  }
  if (keys[GLFW_KEY_D] || keys[GLFW_KEY_RIGHT]) {
    side = side + 1;
  }

  if (front != 0 || side != 0) {
    camera.move(side, front, deltaTime);
  }
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode) {
  if((key == GLFW_KEY_ESCAPE || key == GLFW_KEY_Q) && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);

  if(action == GLFW_PRESS)
    keys[key] = true;
  else if(action == GLFW_RELEASE)
    keys[key] = false;	
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
  if(firstMouse) {
    lastX = xpos;
    lastY = ypos;
    firstMouse = false;
  }

  GLfloat xoffset = xpos - lastX;
  GLfloat yoffset = lastY - ypos; 
  
  lastX = xpos;
  lastY = ypos;

  camera.rotate(xoffset, yoffset);
}	

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
  camera.zoom((GLfloat) yoffset);
}

GLuint loadTexture(std::string path, GLboolean alpha) {
  GLuint texture;
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  int width, height;
  unsigned char* image = SOIL_load_image(path.c_str(), &width, &height, 0,
                                         alpha ? SOIL_LOAD_RGBA : SOIL_LOAD_RGB);
  glTexImage2D(GL_TEXTURE_2D, 0, alpha ? GL_RGBA : GL_RGB,
               width, height, 0,
               alpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, image);
  glGenerateMipmap(GL_TEXTURE_2D);
  SOIL_free_image_data(image);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                  alpha ? GL_CLAMP_TO_EDGE : GL_REPEAT);	// Horizontal texture wrapping
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
                  alpha ? GL_CLAMP_TO_EDGE : GL_REPEAT);  // Vertical texture wrapping
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                  GL_LINEAR_MIPMAP_LINEAR);               // Texture minimizing
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                  GL_LINEAR);                             // Texture stretching
  glBindTexture(GL_TEXTURE_2D, 0); 

  return texture;
}

RenderTexture makeRenderTexture(GLuint width, GLuint height) {
  // Create and bind framebuffer
  RenderTexture buf;
  glGenFramebuffers(1, &buf.id);
  glBindFramebuffer(GL_FRAMEBUFFER, buf.id);
  
  // Create blank texture that spans entire screen
  glGenTextures(1, &buf.texid);
  glBindTexture(GL_TEXTURE_2D, buf.texid);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height,
               0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glBindTexture(GL_TEXTURE_2D, 0); 

  // Attach texture to framebuffer
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                         GL_TEXTURE_2D, buf.texid, 0);

  // Create renderbuffer
  glGenRenderbuffers(1, &buf.rbo);
  glBindRenderbuffer(GL_RENDERBUFFER, buf.rbo); 
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
  glBindRenderbuffer(GL_RENDERBUFFER, 0);
  
  // Attach renderbuffer to depth & stencil attachments
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT,
                            GL_RENDERBUFFER, buf.rbo);

  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    throw std::runtime_error("Cannot complete Framebuffer!");
  }
  
  glBindFramebuffer(GL_FRAMEBUFFER, 0);  // Unbind framebuffer
  
  // Create quad for rendering to texture
  GLfloat quadVertices[] = {
    // Positions         // Texture Coords (swapped y coordinates to flip texture)
    -1.0f,  1.0f, 0.0f,  0.0f,  1.0f,
    -1.0f, -1.0f, 0.0f,  0.0f,  0.0f,
     1.0f, -1.0f, 0.0f,  1.0f,  0.0f,

    -1.0f,  1.0f, 0.0f,  0.0f,  1.0f,
     1.0f, -1.0f, 0.0f,  1.0f,  0.0f,
     1.0f,  1.0f, 0.0f,  1.0f,  1.0f
  };

  glGenVertexArrays(1, &buf.vao);
  glGenBuffers(1, &buf.vbo);
  glBindVertexArray(buf.vao);
  glBindBuffer(GL_ARRAY_BUFFER, buf.vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
  glBindVertexArray(0);

  return buf;
}
